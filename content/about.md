---
date: "2020-11-17"
title: Chroniques d'un cartographe encarté
---

Nicolas "cartographe encarté" Lambert est ingénieur de recherche CNRS en sciences de l'information géographique. Il est cartographe, membre du parti communiste et du réseau Migreurop. Il est co-auteur d'un manuel de cartographie en 2016 et de Mad Maps en 2019 aux éditions Armand Colin.
