---
title: Mes chers camarades, je pars
author: Nicolas Lambert
date: '2017-01-10'
slug: mes-chers-camarades-je-pars
categories: []
tags: []
description: Article description.
featured: yes
toc: no
featureImage: 
thumbnail: images/scan0055.png
shareImage: images/scan0055.png
codeMaxLines: 10
codeLineNumbers: no
figurePositionShow: yes
---

Mes cher(e)s camarades, mes ami(e)s.  Après de nombreuses hésitations au cours des derniers mois et après avoir beaucoup réfléchi, j’ai fini par prendre une décision. Je l’ai esquissée hier pendant la réunion de circonscription. Ceci n’a pas été facile. Je souhaite prendre de la distance avec le mouvement la France insoumise, qui ne me convient pas. En effet, sous couvert d’assemblées citoyennes et de démocratie participative, FI [La France Insoumise] est selon moi une organisation très centralisée et au final assez peu démocratique. Et je suis convaincu que ce n’est pas de cette façon que nous construirons dans la durée notre « avenir en commun ». Et surtout, le fait de présenter des candidats FI dans toutes les circonscriptions, y compris dans les bastions communistes, est pour moi une ligne rouge qu’il ne m’est pas possible d’accepter et qui en dit beaucoup sur les objectifs politiques du mouvement. Je ne suis pas du tout d’accord avec ça. Et j’espère sincèrement que les choses vont bouger et que des arbitrages constructifs seront faits.

En cohérence, je prend donc également la décision de quitter le parti de gauche qui n’est rien d’autre que le noyau dur de ce mouvement. Pour mémoire, j’ai adhéré au PG en 2009, parce qu’il s’y produisait une dynamique que je croyais nécessaire et utile d’appuyer. Le PG était alors un parti creuset qui posait un acte fort de rupture avec le PS, qui était dans mon esprit comparable, dans une moindre mesure, à la rupture de 1920. Le but du PG était alors d’unir les forces de l’autre gauche au sein du Front de Gauche, c’est à dire, pour faire court, l’alliance avec le parti communiste, dans le but de passer en tête de toute la gauche. Puis, la stratégie a changé. Nous nous sommes probablement perdus dans des alliances hasardeuses ; l’expérience Grenobloise est d’ailleurs en train de tourner au vinaigre. Puis, le PG a fini par être totalement dissous dans cette nouvelle construction électorale qu’est la France insoumise. J’y ai cru un temps en voyant qu’il se produisait une dynamique. Mais aujourd’hui, la dynamique est surtout cristallisée autour de la personne et le talent indéniable de Jean-Luc Mélenchon mais rayonne assez peu au-delà (cf le nombre de personnes assez limité impliqué dans les groupes d’appui).

Je quitte donc le parti de gauche et la France insoumise et j’ai décidé de me rapprocher du parti communiste qui est, vous l’aurez compris, le parti avec lequel je me sens le plus proche d’un point de vue philosophique. Aujourd’hui, alors que je viens d’avoir 40 ans, le PCF est le cadre dans lequel j’ai envie de militer il incarne pour moi, le mieux l’idée de commun et de collectif, idée plus que jamais pertinente dans nos (dis)sociétés de plus en plus atomisées et individualistes.

En tout cas, mes ami(e)s, il ne faut rien lâcher. Toujours se battre pour ses convictions et surtout, créer du collectif et des convergences. Quoi qu’il arrive, nous restons dans le même camp, celui du progrès social. Alors créons du commun, des convergences et des dynamiques. Et menons la bataille culturelle sans jamais céder de terrain. Mes chers amis, hauts les cœurs ! 

Fraternellement

Nicolas
