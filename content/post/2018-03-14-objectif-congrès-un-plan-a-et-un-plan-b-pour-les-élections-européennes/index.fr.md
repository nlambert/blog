---
title: 'Objectif congrès : un plan A et un plan B pour les élections européennes'
author: Nicolas Lambert
date: '2018-03-14'
slug: objectif-congrès-un-plan-a-et-un-plan-b-pour-les-élections-européennes
categories: []
tags:
  - congres
  - PCF
description: Article description.
featured: yes
toc: no
featureImage: 
thumbnail: images/option3.png
shareImage: images/option3.png
codeMaxLines: 10
codeLineNumbers: no
figurePositionShow: yes
---

**Quelle stratégie de rassemblement pour les communistes ? Quelles sont les forces disponibles à gauche ? Quel plan de bataille pour les élections européennes. Pour tenter de tracer les différents scénarios possibles, je livre ici plusieurs infographies résumant, à mon avis, les possibilités que nous avons devant nous.**

Le parti communiste est une force de rassemblement. Front populaire, programme commun, gauche plurielle, Front de Gauche, le PCF a toujours ancré son action politique dans des processus unitaires larges. Car rassembler les forces disponibles, à différentes échelles, sur un cap clair de rupture avec les politiques d’austérité, que ce soit en France ou en Europe, est en effet le seul moyen de construire une alternative progressiste majoritaire. Dans la rue et dans les urnes, la convergence des forces anti-austéritaires rassemblées sur un contenu et des objectifs précis (il n a jamais été question de juxtapositions d’étiquettes sans contenus), est le levier nécessaire pour retrouver le chemin des conquêtes sociales. Car dans l’histoire de France, rien n’a jamais été conquis pour les classes populaires, dans la division.

La liste des partis et mouvements avec lesquels nous sommes successibles de pouvoir travailler est connue : la France Insoumise, Génération.s, le NPA, EELV, etc. Le parti communiste lance d’ailleurs régulièrement des appels à ces différentes forces. Dans cet état d’esprit, Pierre Laurent a lancé d’ailleurs aujourd’hui, soit quelques jours avant la manifestation du 22 mars, un appel à l’élaboration d’une tribune commune des dirigeants de gauche opposés à la “réforme” de la SNCF. Un appel à l’unité récurant et opiniâtre du secrétaire national du PCF.

![images/paysage.png]

ace ce paysage politique en recomposition, et pour tenter de tracer différents scénarios stratégiques possibles, je livre ici 5 infographies résumant, à mon avis, les possibilités que nous avons devant nous.

### Option 1 : L’union des communistes

![images/option1.png]

Face à des PS ou ex PS qui occupent intégralité de l’espace médiatique et politique de la gauche, la première option que je livre ici, consiste à remettre sur la table la question de l’unité des communistes. En effet, Jean-Luc Mélenchon (19,58%), Emmanuel Macron (24,01%) et Benoit Hamon (6,36%) qui sont tous les trois issus du PS et dont les idées ont longtemps cohabité dans le même parti dans des tendances différentes, ont pesé à eux seuls 50% des suffrages à l’élection présidentielle, laissant quasiment sans voix la gauche non “socialiste”. Faire réémerger médiatiquement et dans la société une parole communiste forte en l’unissant, est donc une option qu’il nous faut sérieusement envisager. Olivier Besancenot, fait d’ailleurs en ce moment des gestes d’unité qu’il serait peut être intéressant de saisir au vol et de pousser plus loin. Évidement, si cette solution peut paraitre évidente, tout le monde sait que c’est loin d’être la plus facile...

### Option 2 : Un front de gauche renouvelé.

![images/option2.png]

Petit rappel historique... Quelques années après les luttes victorieuses et unitaires contre le TCE en 2005, le PCF lançait le 24 octobre 2008 « un appel aux forces politiques et sociales » pour une « refondation de la construction européenne » à constituer des listes communes aux européennes. En décembre 2008, le 34e Congrès du PCF se prononçait alors pour des « constructions unitaires avec des cadres, des fronts, des alliances adaptés aux contenus portés et aux échéances affrontées ». Ainsi sécurisé par une promesse d’alliance pour les élections à venir, Jean-Luc Mélenchon quittait alors le PS le 1er février 2009 et créait le Parti de gauche avec Marc Dolez. Dans la foulée, Christian Picquet (aujourd’hui au PCF) quittait également le NPA et annonçait la création de la Gauche Unitaire (8 mars 2009). Ces trois forces convergent alors dans le Front de Gauche, qui recueille 6,45 % des voix aux élections européennes. Un succès. Cette histoire collective se poursuivra et grandira jusqu’aux élections régionales de 2015.

Mais depuis la dernière campagne présidentielle, le Front de Gauche a volé en éclat. Si le PCF a, comme en 2012, parrainé et soutenu Jean-Luc Mélenchon et en a fait son candidat, la campagne n’a cette fois-ci pas été collective et les législatives se sont, dans bien des endroits, transformées en une véritable guerre de tranchées. Aujourd’hui, communistes et insoumis siègent dans deux groupes bien distincts à l’assemblée nationale. Pourtant, s’il existe bien évidemment des différences programmatiques entre FI et PCF (CSG, migrants, droits dans l’entreprise, dé-financiarisation de l’économie, lutte contre le chômage, nucléaire, etc.), on peut quand même constater une large communauté de pensée et d’analyse entre les deux forces, ce qui pourrait permettre de converger utilement. Le PCF avec son ancrage territorial et dans les luttes, LFI avec son impact médiatique et numérique, sont en réalité deux forces extrêmement complémentaires.

Malheureusement, La France Insoumise considère aujourd’hui que toutes les alliances, y compris avec les anciens alliés du Front de Gauche, sont des “étouffoirs” et de la “tambouille”. Notre candidat a la présidentielle a même qualifié récemment le PCF de “lamentable secte”. Travailler ensemble semble donc aujourd’hui totalement impossible. L’élan du Front de Gauche est brisé. Prenons en acte.

### Option 3 : Rassembler largement la gauche (front populaire)

![images/option3.png]

Une troisième option, qui est probablement d’avantage une solution de second tour (pour les scrutins à deux tours), consiste à tenter d’agréger la gauche au sens large et créer ainsi une sorte de front populaire large et divers. Évidement, cette solution se heurte a une difficulté de poids, puisque le clivage gauche-droite, limite qu’il est évidemment impossible de franchir à travers nos alliances, traverse aujourd’hui de part en part certains partis.

### Option 4 : Prendre acte de ceux qui ne veulent pas

![images/option4.png]

De toute façon, une telle alliance aussi large n’a absolument aucune chance d’advenir puisque jamais LFI n’acceptera d’entrer dans un tel dispositif. Une autre option consisterait alors d’effectuer ce rassemblement large, sans la France insoumise. Mais à y bien réfléchir, que gagnerions-nous à long terme à troquer une alliance avec un ancien PS par une alliance avec un autre ancien PS ? Il me semble que la réponse est dans la question. Alors que faire ?

### Plan A et plan B pour les élections européennes.

Ma proposition est la suivante. Tout d’abord, nous devons regarder l’échéance qui est devant nous : les élections européennes. Cette échéance, nous devons l’aborder avec sérieux et détermination. Pour cela, nous devons définir très vite un socle de propositions concertes, et désigner rapidement 3 ou 4 personnes (à parité) successibles de les porter. Bref, constituer une liste 100% communiste, ouverte comme toujours aux non encartés qui se sentent proches de nous et qui se revendiquent encore aujourd’hui de l’idée du Front de Gauche. En d’autres termes, nous devons commencer à mener campagne le plus tôt possible, mettre en avant nos idées et les visages de celles et ceux qui vont les défendre. Bref, reconquérir un peu de visibilité et ré-ancrer nos idées dans la société.

![images/option5-1.png]

Mais attention, cette stratégie de campagne autonome n’est en aucun cas un renoncement aux processus de rassemblement dont nous nous réclamons tant. Car cette campagne 100% communiste, nous devons la mener sans jamais cesser d’appeler à l’unité et au travail commun. Bref, nous devons mener campagne la main tendue. En d’autres termes, nous devons déployer une stratégie du type plan A - plan B (puisque c’est la mode). Notre plan A, c’est le rassemblement le plus large possible des forces de gauche déterminées à s’opposer aux politiques austéritaires. C’est notre priorité. Notre plan B, c’est une liste communiste sans alliances.

Pourquoi faire cela ? Car le pire serait d’attendre, d’attendre et d’attendre encore que nos alliés potentiels se décident et, en cas de refus, improviser une campagne bricolée au dernier moment. Si le PCF ne veut pas à nouveau être invisibilisé et passer sous le tapis, nous devons nous préparer à aller à la bataille comme si nous étions seuls, et entrer dès maintenant en campagne ! Et si jamais des processus unitaires surgissent (nous ne manquerons pas de les susciter), nous nous y engouffrerons.

