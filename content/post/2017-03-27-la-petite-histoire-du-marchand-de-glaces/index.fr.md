---
title: La petite histoire du marchand de glaces
author: Nicolas Lambert
date: '2017-03-27'
slug: la-petite-histoire-du-marchand-de-glaces
categories: []
tags:
  - Economie
  - Théorie des jeux
description: Article description.
featured: yes
toc: no
featureImage: 
thumbnail: images/marchand-glace-1.jpg
shareImage: images/marchand-glace-1.jpg
codeMaxLines: 10
codeLineNumbers: no
figurePositionShow: yes
---

**La théorie des jeux est une approche mathématique de l’économie qui consiste, en étudiant les comportements des individus, à présenter les problèmes économiques sous forme de jeux stratégiques. De nombreux exemples sont énoncés pour illustrer cette théorie. Le problème du marchand de glaces est l’un des plus célèbres. Je l’ai un peu revisité.**

La théorie des jeux est une approche mathématique de l’économie qui consiste, en étudiant les comportements des individus, à présenter les problèmes économiques sous forme de jeux stratégiques. De nombreux exemples sont énoncés pour illustrer cette théorie. Le problème du marchand de glaces est l’un des plus célèbres. Je l’ai un peu revisité.

![images/job-ete-plage-glace.jpg]

Le principe est simple. Deux marchands de glaces ont installé leurs stands sur une plage. Les clients potentiels sont répartis uniformément sur la plage. Les marchands vendent des glaces de même qualité et au même prix. Ils ont pour objectif de maximiser leurs profits en servant le plus de clients possibles. Au départ, les deux marchands de glaces, qui se connaissaient bien, avaient décidé de s’entendre et de coopérer. Ainsi, le premier marchand occupait la partie gauche de la plage tandis que le deuxième était présent sur la partie droite (situation 1). Dans cette configuration, les deux marchands vendent sur un territoire de taille identique tout en minimisant la distance que doit faire un client pour aller chercher sa glace. Les profits des deux marchands sont identiques. Les clients sont contents. Un beau jour, le marchand de glaces numéro 1, poussé par l’appât du gain,  décide d’augmenter son nombre de clients en se rapprochant du centre de la plage pour contrôler un territoire plus grand. L’autre marchand de glaces, qui n’a rien demandé à personne, voit ainsi son territoire réduit, avec moins de clients potentiels. Pour ne pas voir son chiffre d’affaire diminuer, il n’a qu’une solution, il se rapproche également du centre de la plage pour récupérer sa partie de plage et ainsi rétablir l’équilibre. Du coup, le premier marchand se rapproche encore plus du centre, suivi par le second, et ainsi de suite jusqu’à ce que les deux marchands se retrouvent au milieu de la plage (situation 2). L’équilibre est rétabli. Chaque marchand, a comme au début de l’histoire, le même chiffre d’affaire. En théorie des jeux, c’est un équilibre de Nash. La situation est en équilibre car si un des deux marchands de glaces décide de s’éloigner du centre, il voit ainsi son territoire et son nombre le client potentiel réduit. Donc, s’ils ne se décident pas à coopérer et à se faire confiance pour revenir à la situation d’origine, personne ne bouge.

![images/marchand-glace-1.jpg]

C’est un équilibre non-optimal. Alors que dans la situation de départ, les deux marchands gagnaient autant d’argent l’un que l’autre, à présent, c’est toujours le cas, mais avec un chiffre d’affaire plus faible. Car, si on continue de supposer que les clients se dirigent vers le marchand de glaces le plus proche, il n’est pas sur que ceux qui sont situés à l’extrémité de la plage marcheront des kilomètres pour rejoindre le centre de la plage. Vraisemblablement, ils se passeront de glaces. Du coup, les deux marchands de glaces voient leurs chiffres d’affaire réduits. Ils ne sont pas contents. Et les clients non plus.