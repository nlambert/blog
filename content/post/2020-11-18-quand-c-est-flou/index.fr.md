---
title: Quand c'est flou...
author: Nicolas Lambert
date: '2018-09-14'
slug: quand-c-est-flou
categories: []
tags: [migrations]
description: Article description.
featured: yes
toc: no
featureImage: 
thumbnail: images/chien-flou.jpg
shareImage: images/chien-flou.jpg
codeMaxLines: 10
codeLineNumbers: no
figurePositionShow: yes
---

**Alors que plusieurs députés de la France Insoumise ont décidé de ne pas venir cette année à la fête de l’Humanité à cause de désaccords sur la question migratoire, voici un petit dialogue (presque) fictif pour montrer ce qui coince...**

**Le journaliste** : La position de la France Insoumise n’est pas toujours très claire. Trouvez-vous que la France prend sa part dans l’accueil des migrants ?
L’insoumis : La position de la France Insoumise est extrêmement claire.

**Le journaliste** : Éclairez-nous, quel accueil proposez-vous pour les réfugiés ?

**L’insoumis** : Nous avons un livret programmatique et rédigé plein de propositions pendant les débats sur la loi asile immigration. Ce que nous disons, c’est que la question migratoire n’est jamais prise par le bon bout. On ne parle que des conséquences et jamais des causes.

**Le journaliste** : Mais concrètement, à part dire qu’il faut arrêter les guerres, quel accueil ?

**L’insoumis** : Pour ce qui est de votre question précise.... Bon, non mais attendez, c’est un tout et il faut être sérieux. On ne discute jamais de pourquoi les gens sont poussés à la sortie. Je vous alerte sur ce point, toute migration est un exil forcé. Soyons sérieux. Si on ne s’attaque pas au réchauffement climatique, d’ici 2050, ce sont des centaines de millions de personnes qui vont être poussées à la sortie.

**Le journaliste** : ...

**L’insoumis** : Par conséquent, une politique rationnelle en matière de migrations, c’est de prendre les choses dans leur globalité. Ca passe à la fois par le traitement des causes, les guerres, le réchauffement climatique, ....

**Le journaliste** : Oui... Et ?

**L’insoumis** : Parlons aussi des conséquences des politiques néolibérales qui sont menées en Afrique. Vous savez, le libéralisme économique, ca conduit à ce que le poulet subventionné par l’Europe arrive sur le marché Nord Africain, coûte moins cher que le poulet qui est élevé sur place. Il faut s’interroger sur tout ca. Le CETA, le TAFTA. Tout ca c’est la même logique.

**Le journaliste** : ...

**L’insoumis** : Les propositions de la France insoumise sont raisonnables. Crédibles. Nous proposons un chemin pour sortir de l’impasse.

**Le journaliste** : Et l’accueil ?

**L’insoumis** : J’ai déjà répondu à cette question

**Le journaliste** : ...

**L’insoumis** : Que voulez vous monsieur le journaliste. Que voulez vous qu’on fasse une fois qu’ils sont là ? On ne va pas le jeter à la mer. Donc on les accueille. C'est notre devoir d'humanité de les accueillir. Ce sont nos semblables. Mais il faut voir les choses dans leur globalité. Nous n’avons jamais cru à la liberté de circulation. Nous n’allons pas commencer aujourd’hui. Il faut en finir avec la bonne conscience de gauche sur la culture de l’accueil.

**Le journaliste** : Merci monsieur l’insoumis

**L’insoumis** : Au revoir monsieur le journaliste

---

Vous l’aurez compris, cette rhétorique très maligne permet de faire cohabiter des positions antagonistes, pour que chacun puisse entendre ce qu’il a envie d’entendre. C’est comme la stratégie plan A plan B qui permet de réunir ceux qui veulent rester dans l’Union européenne et ceux qui veulent la quitter. C'est la même logique. Ici, sur la question migratoire, le discours qui commence systématiquement par évoquer les causes, permet de véhiculer à la fois un discours de fermeté en disant que l’objectif c’est "d’assécher le flux" (dixit Kuzmanovic, candidat FI aux prochaines élections européennes) tout en disant en même temps qu’on tend la main à ceux qui arrivent (puisque ils sont là après tout). Bref, à la fois l’ouverture et la fermeture. Une martingale médiatique gagnante à tous les coups. Jusqu'à un certain point. Car comme l’écrit Roger Martelli dans le magazine Regards, cette position tourne le dos au mouvement réel. Au mouvement des associations et de tous ceux qui luttent sur le terrain. Et que feraient ceux qui arrivent au pouvoir sur cette base ? Concrètement ? On ne le sais pas. C’est flou. Et comme dirait l’autre, quand c’est flou, c’est qu’y’a un loup !


(*) dialogue inspiré de cette interview datée du 4 juillet dernier.
http://www.europe1.fr/emissions/linvite-de-patrick-cohen/adrien-quatennens-emmanuel-macron-meprise-le-parlement-3701081
