---
title: Pour une nouvelle boussole politique
author: Nicolas Lambert
date: '2018-02-21'
slug: pour-une-nouvelle-boussole-politique
categories: []
tags:
  - infographie
description: Article description.
featured: yes
toc: no
featureImage: 
thumbnail: images/esperance-de-vie-1.png
shareImage: images/esperance-de-vie-1.png
codeMaxLines: 10
codeLineNumbers: no
figurePositionShow: yes
---

**Quel est le lien entre Justin Timberlake et le capitalisme ? Quel est le rapport entre un blockbuster et un rapport de l’INSEE ? Qu'ont à voir ensemble les inégalités de richesses et l’espérance de vie à la naissance ? Et quel est le point commun entre une boussole et mai 68 ? Pour le savoir, la réponse se trouve dans cet article :)**

### Quand Justin Timberlake pourfend le capitalisme

Avez-vous vu le film Time Out ? Si non, en voici le pitch... Time Out est un film de science-fiction réalisé par Andrew Niccol, sorti en 2011. L’histoire se déroule en 2070, dans le quartier pauvre de Dayton, dans lequel Justin Timberlake (alias Will Salas) est un travailleur pauvre. Un prolétaire qui travaille pour survivre. Littéralement. Car dans ce monde futuriste et dystopique, les êtres humains ont été génétiquement modifiés, ils ne vieillissent plus au delà de 25 ans, mais à partir de cet âge, un compteur greffé sous la peau de leur à l'avant-bras s’enclenche, un compte à rebours macabre décomptant le temps qu’il leur reste à vivre. A 25 ans, seule une année de temps de vie leurs est créditée. Quand le compteur tombe à zéro, ils meurent. Pour gagner du temps de vie supplémentaire, il faut alors aller travailler pour gagner à la sueur de son front, un salaire “temporel”. Mais d’autres méthodes sont possible pour allonger sa durée de vie : le don (on peu donner librement du temps à quelqu’un d’autre, un ami, un proche) ou le vol (en connectant son compteur à celui d’une autre personne contre son gré, jusqu’à ce qu’elle meure). Dans ce film, les inégalités ont également une dimension géographique très marquée puisque pour se déplacer et accéder aux quartiers riches, il faut franchir de nombreux péages et y laisser à chaque fois, plusieurs mois de vie. Un véritable monde d’apartheid social et spatial.

![images/time-out1.png]

Évidement, dans un tel système politique, on comprend bien que si certains sont obligés de s’employer au quotidien pour grappiller ne serait-ce qu’un jour de vie supplémentaire leur permettant de retourner travailler le lendemain, certains peuvent quant à eux capitaliser des milliers d’années de vie, parce qu’ils sont bien nés, qu’ils sont proches du pouvoir ou qu’ils possèdent les moyens de production. In fine, ce film décrit un système où le capitalisme permet aux privilégiés l’immortalité tandis que les ouvriers mettent à contribution leur force de travail pour survivre au quotidien. Un rêve non ?

Bien sur, vous imaginez bien que dans ce blockbuster américain, le héros, tel un révolutionnaire du futur vent debout contre les injustices, va partir à l’assaut de ce système pour tenter de le faire voler en éclat. Vous vous en doutiez, hein ?

### De la science fiction ?

Le monde dans lequel nous vivons est-il si éloigné de celui décrit par Andrew Niccol dans son film ? Pas si sur... Une étude récente de l’INSEE portant sur l’espérance de vie par niveau de vie arrive à la conclusion suivante : plus on est aisé, plus on vit longtemps. Et cela en particulier vrai chez les hommes. En 2012-2016, parmi les 5 % les plus aisés (dont le niveau de vie moyen est de 5 800 euros par mois), l'espérance de vie à la naissance des hommes est de 84,4 ans. Mais pour ceux qui font parti des 5 % les plus pauvres (dont le niveau de vie moyen est de 466 euros par mois), l'espérance de vie tombe à 71,7 ans. Entre les riches et les pauvres, il y a donc 12,7 ans écart d’espérance de vie. Presque 13 ans. Aux alentours d’un niveau de vie de 1 000 euros par mois, 100 euros supplémentaires correspondent à 0,9 an d’espérance de vie en plus. Dans notre France d'aujourd’hui, tout comme dans le film Time Out, l’argent se converti donc presque mécaniquement en temps de vie supplémentaire.

Parmi les éléments explicatifs avancés par l’INSEE, le premier est l’inégal accès aux soins, puisque, précise le rapport, des difficultés financières peuvent en effet limiter l’accès aux services de santé. Rappelons que 11 % des adultes parmi les 20 % les plus modestes disent avoir renoncé pour des raisons financières à consulter un médecin au cours des 12 derniers mois, contre 1 % des adultes parmi les 20 % les plus aisés. Deuxième élément explicatif, l’exposition aux risques professionnels. Les ouvriers sont en effet davantage soumis à certains risques que les cadres : accidents, maladies, exposition à des produits toxiques, etc. Troisième éléments, les modes de vies à risque. 39 % des personnes âgées de 15 à 64 ans sans diplôme fument quotidiennement, contre seulement 21 % des diplômés du supérieur.

Au final, ce rapport démontre que le niveau de vie a un impact direct sur la durée de la vie elle même. Des années volées aux plus pauvres, du fait de leur condition sociale. Inacceptable !

![images/esperance-de-vie-1.png]

### Pour une nouvelle boussole politique

Bien plus que le niveau de PIB, le taux de croissance et même le taux de chômage, ce critère - l’écart d’espérance de vie entre les riches et les pauvres - doit devenir notre boussole politique. Un moyen de guider nos actions et d’évaluer concrètement leur efficacité. Car faire mourir des hommes et des femmes plus tôt en leur imposant des conditions de vie dégradées, c’est l’inégalité ultime. Une véritable dystopie pourtant bien réelle.

Pour changer cet état de fait, de nombreuses propositions sont d’ailleurs d'ores et déjà sur la table. J’en reprends ici 4 qui me semblent centrales, sans prétendre évidemment à exhaustivité.

1) Mieux partager la richesse produite en réduisant les écarts de revenus entre les riches et les pauvres. Certains proposent des écarts de 1 à 10 ou même de 1 à 20. Pour ma part, je considère que 4 tranches de revenus sont amplement suffisantes, de 1500 € (c’est-à-dire au dessus du SMIC actuel) à 6000 € environ. A quoi bon aller au delà ? Je rappelle que les 5% les plus riches gagnent en moyenne 5 800 euros par mois. Une telle mesure n’impacterait donc à la baisse que les revenus des ultra riches.

2) Sécuriser les parcours professionnels pour que personne ne se retrouve sur le carreau. Pour cela, plusieurs propositions sont en débat, comme le salaire à vie (Bernart Friot) ou la sécurisation de l’emploi et de la formation qui se trouve dans le programme du parti communiste. Dans les deux cas, l’idée est de permettre aux adultes de vivre bien, sans crainte de déclassement, qu’ils soient en activité professionnelle ou en formation. Une sécurisation professionnelle permettant à chacun de construire dignement sa vie sans aléas.

3) Mettre en place une protection sociale de haut niveau pour toutes et tous. Santé, retraite, famille, dépendance... Aller vers une sécurité sociale qui rembourse à 100% et qui renouerait avec la conception d’Ambroise Croizat, c’est-à-dire un système géré par les salariés eux-mêmes et financé intégralement pas les cotisations. Face aux déserts médicaux, une amélioration de l’accès aux soins est également indispensable. Il faudra pour cela développer un véritable maillage de services publics de santé ambulatoire sur tout le territoire (e.g. des centres de santé municipaux).

4) Émanciper le travail en étendant la sphère de la souveraineté populaire de la cité à l’entreprise. Cela doit passer à terme par la réappropriation des moyens de production, qui reste plus que jamais un objectif politique. Mais cela peut se faire de façon graduelle. Par exemple, à travers une disposition législative simple qui autoriserait un droit de préemption réel des salariés en cas de dépôt de bilan ou de fermeture de site. Cela permettrait d’instaurer une propriété d’usage à la disposition des (ex-) salariés pour construire un monde du travail libéré de l’exploitation capitaliste.

Faire converger les luttes pour renouer avec les conquêtes ouvrières

Même si 35 ans de néolibéralisme donnent l’impression inverse, la régression sociale n’est pas une fatalité. Des luttes victorieuses sont possibles. Ensemble, unis, déterminés, nous sommes capables de renouer avec le fil de l’histoire ouvrière et d’aller arracher de nouvelles conquêtes sociales. C’est à portée de main si nous nous en donnons réellement les moyens. Et face à un Macron déterminé à dérouler son programme d’atomisation de la société à coups de 49-3 ou de votes bloqués, seule une grève d’ampleur nationale pourra permettre de le stopper dans son élan. Il faut donc sortir l’artillerie lourde et fêter les 50 ans de mai 68 comme il se doit. Certes, cela ne se décrète pas, et une grève générale dépend de l'engagement de toutes et tous. Mais pensez-y. Ne serait-ce pas chouette d'arracher aux exploiteurs 13 années de vie supplémentaires ?

Source : https://www.insee.fr/fr/statistiques/3319895