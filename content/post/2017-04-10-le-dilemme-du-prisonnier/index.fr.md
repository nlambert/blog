---
title: Le dilemme du prisonnier
author: Nicolas Lambert
date: '2017-04-10'
slug: le-dilemme-du-prisonnier
categories: []
tags:
  - Théorie des jeux
description: Article description.
featured: yes
toc: no
featureImage: 
thumbnail: images/hands-behind-prison-bars-vector-art-300x180.png
shareImage: images/hands-behind-prison-bars-vector-art-300x180.png
codeMaxLines: 10
codeLineNumbers: no
figurePositionShow: yes
---

**Le dilemme du prisonnier est un exemple célèbre de la théorie des jeux. John Nash, (Prix Nobel 1994) a montré très tôt qu’alors qu’il existe des solutions collectives préférables, la compétition entre individus rationnels aboutit au pire des choix possibles.**

Après la petite histoire du marchand de glace [voir], je reviens avec ce nouvel exemple bien connu de la théorie des jeux qui démontre que la coopération est préférable à la compétition. 

Mettez-vous en situation : Vous êtes un criminel arrêté par la police et enfermé en isolement. Vous avez un complice, arrêté et enfermé lui aussi. Vous ne pouvez pas communiquer avec lui. Voici les règles.  Si l’un avoue et pas l’autre, celui qui a avoué sera emprisonné à perpétuité tandis que l’autre sera libéré. Si vous niez tous les deux, vous écopez tous les deux de 20 ans de prison. Si vous avouez tous les deux, le jugement est plus clément, vous écopez chacun de 10 ans de prison.

Dilemme… Avouer ou pas avouer ?

Il y a 4 cas de figures :

– Nous avouons tous les deux, j’écope de 10 ans de prison.
– J’avoue et l’autre nie, je suis enfermé à perpétuité.
– Nous nions tous les deux, j’écope de 20 ans de prison
– Je nie et l’autre avoue, je suis libéré

Donc, si mon complice avoue, j’ai le choix entre avouer (10 ans) et nier (libéré), donc je nie. Si mon complice n’avoue pas, j’ai le choix entre avouer (perpétuité) ou nier (10 ans), donc je nie. Le seul choix rationnel est donc de ne pas avouer. Le problème est que mon complice fait le même raisonnement et nous écopons tous les deux de 20 ans de prison. Nous aurions pu amoindrir notre sanction si nous avions pu nous entendre (i.e. coopérer) et avouer tous les deux.

Le dilemme du prisonnier est un exemple célèbre de la théorie des jeux. John Nash, (Prix Nobel 1994) a montré très tôt qu’alors qu’il existe des solutions collectives préférables, la compétition entre individus rationnels aboutit au pire des choix possibles. C’est un résultat essentiel de l’économie la plus orthodoxe qui soit puisqu’il démontre l’inefficacité de la « main invisible du marché » . Alors ne faisons pas comme si ce résultat essentiel n’avait jamais été démontré…
