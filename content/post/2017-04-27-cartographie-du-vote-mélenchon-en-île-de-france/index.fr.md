---
title: Cartographie du vote Mélenchon en Île-de-France
author: Nicolas Lambert
date: '2017-04-27'
slug: cartographie-du-vote-mélenchon-en-île-de-france
categories: []
tags:
  - cartographie
description: Article description.
featured: yes
toc: no
featureImage: 
thumbnail: images/vote-jlm.png
shareImage: images/vote-jlm.png
codeMaxLines: 10
codeLineNumbers: no
figurePositionShow: yes
---

**Insert Lead paragraph here.**

![images/vote-jlm.png]

Quelques jours après le premier tour, et alors que la perspective du second est loin d'être réjouissante, je vous livre ici une carte du vote en faveur Jean-Luc Mélenchon à l'échelle de la région l'île-de-France. Pour sa réalisation, j'ai utilisé deux variables : le nombre de voix pour le candidat et le nombre de suffrages exprimés à l'échelle des communes. Puis, les données ont été représentées via la méthode des potentiels pour permettre de délivrer un message spatial "sans bruit". L'opposition entre la couleur rouge et la couleur verte s'effectue de part et d'autre de la valeur 19.56%, c'est à dire le score moyen de Jean-Luc Mélenchon à l'échelle nationale (contre 21.75% en île-de-France). Voilà pour la cuisine technique.

Que nous montre cette carte ?

Tout d'abord, rappelons qu'à l'échelle du pays, dans les villes à direction communiste (ou Front de Gauche), le score de Jean-Luc Mélenchon est supérieur de 11 points à la moyenne nationale (30, 6%). L'empreinte communiste (pour faire référence à Roger Martelli) a donc été un levier determinant, permettant d'emmener la candidature de Jean-Luc Mélenchon encore plus loin. A l'échelle de l'Île-de-France , le poids de cette empreinte dans le score de Jean-Luc Mélenchon se voit à l’œil nu. Dans les villes qui ont été marquées ou sont encore dirigées par le parti communiste, les scores sont impréssionnants et dépassent parfois les 40% : Gennevilliers (47%), L'Ile-Saint-Denis (46%), Villetaneuse (45%), La Courneuve (44%), Saint-Denis (43%), Bobigny (43%), Grigny (42%), Valenton (42%), Stains (41%), Aubervilliers (41%), Bagnolet (41%), Montreuil (40%). Le seul département où Mélenchon arrive en tête du premier tour est le département de la Seine-Saint-Denis, dont il est inutile de rappeler l'histoire, avec un peu plus de 34% des suffrages. 

Mais, au delà de ces villes "rouges" qui ont fait la fierté de la classe ouvrière, on voit aussi apparaitre sur la carte des communes traditionnellement socialistes qui ont basculé en 2017 vers un vote massif en faveur du candidat de la France insoumise. C'est le cas des villes nouvelles d’Evry et de Sénart, aujourd'hui regroupées au sein du Grand Paris Sud. Dans cette nouvelle agglomération taillée sur mesure pour Manuel Valls, Jean-Luc Mélenchon recueille 35% des suffrage à Evry, 31% à Corbeil-Essonnes, 30% à Moissy-Cramayel et Savigny-le-Temple et 29% au Mée-sur-Seine. Les élections legislatives vont être musclées...

Au final,  on voit donc se dessiner une ceinture rouge élargie qui s'étend de Mantes-la-Jolie au nord-ouest, jusqu'à l'agglomération de Grand Paris Sud, au sud-est. Voilà de quoi construire l'avenir (en commun) avec enthousiasme. 