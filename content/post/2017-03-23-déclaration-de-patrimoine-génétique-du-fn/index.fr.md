---
title: Déclaration de patrimoine (génétique) du FN
author: Nicolas Lambert
date: '2017-03-23'
slug: déclaration-de-patrimoine-génétique-du-fn
categories: []
tags:
  - FN
  - Fascisme
  - RN
  - Le Pen
description: Article description.
featured: yes
toc: no
featureImage: 
thumbnail: images/logo-msi-fn-2.jpg
shareImage: images/logo-msi-fn-2.jpg
codeMaxLines: 10
codeLineNumbers: no
figurePositionShow: yes
---

**Non, le FN n’est pas un parti comme les autres. Ce parti serait un ovni politique hors du système déconnecté de toute histoire politique. Évidemment, il n'en est rien comme le montre l’emblème même de ce parti, très largement inspiré du logo du parti fasciste italien : le MSI.**

Non, le FN n’est pas un parti comme les autres. Fondé en 1972 par Jean-Marie Le Pen et dirigé aujourd'hui par sa fille Marine, le Front National est avant tout une histoire de famille. Classé comme parti d’extrême droite par la plupart des historiens et philosophes, cette appellation est contestée par les dirigeants du FN. Jadis, considérant que son parti était économiquement de droite et socialement de gauche (comme si on pouvait déconnecter les 2 aspects), Jean-Marie Le Pen préférait définir le FN comme appartenant à «la vraie droite», ou à la «droite nationale». Aujourd'hui, Marine Le Pen considère plutôt que le FN est «ni de droite, ni de gauche», clivage qu'elle trouve dépassé. Bref, ce parti serait un ovni politique hors du système déconnecté de toute histoire politique. Évidemment, il n'en est rien comme le montre l’emblème même de ce parti, très largement inspiré du logo du parti fasciste italien : le MSI. 

![images/logo-msi-fn-2.jpg]

Non, le FN n’est pas un parti comme les autres. Fondé en 1972 par Jean-Marie Le Pen et dirigé aujourd'hui par sa fille Marine, le Front National est avant tout une histoire de famille. Classé comme parti d’extrême droite par la plupart des historiens et philosophes, cette appellation est contestée par les dirigeants du FN. Jadis, considérant que son parti était économiquement de droite et socialement de gauche (comme si on pouvait déconnecter les 2 aspects), Jean-Marie Le Pen préférait définir le FN comme appartenant à «la vraie droite», ou à la «droite nationale». Aujourd'hui, Marine Le Pen considère plutôt que le FN est «ni de droite, ni de gauche», clivage qu'elle trouve dépassé. Bref, ce parti serait un ovni politique hors du système déconnecté de toute histoire politique. Évidemment, il n'en est rien comme le montre l’emblème même de ce parti, très largement inspiré du logo du parti fasciste italien : le MSI. 

Nous le voyons, alors que Marine Le Pen prétend qu’elle n’a rien à voir avec le fascisme et ses symboles, le logo de son parti montre sans l'ombre d'un doute une filiation idéologique directe avec l'histoire de l'extreme droite européenne. Et puis les signes allant dans ce sens sont nombreux. Il y a par exemple les meetings que firent ensemble Jean Marie Le Pen et le fasciste Giorgio Almirante. Ou encore le groupe Identité Tradition Souveraineté dont faisait parti Marine Le Pen au parlement européen avec la petite fille de Mussolini qui refuse que l'on rejette l'héritage mussolinien. Il y a aussi bien sur une multitude de photos douteuses (voir infra). Et plus recensement, il y a eu cette rencontre à Coblence en Allemagne, ville où s’étaient réfugiés les aristocrates refusant l’abolition des privilèges du temps de la révolution française, entre Marine le Pen et plusieurs partis xénophobes d’extrême droite allemand, italien, autrichien et néerlandais. Tout un symbole. Quoi qu’en disent ses partisans, le FN est donc bien un parti d’extrême droite dont la filiation idéologique ne peut pas être glissée sous le tapis. Ceci ne doit jamais être banalisé.

![images/maxresdefault.jpg]

Mais le FN aurait, nous dit-on, effectué un virage social. Il serait dorénavant le parti des classes populaires. Et bien non. Car quand on regarde les constantes programmatiques du FN au fil des ans, une cohérence se dégage qui forge la carte d’identité de ce parti : chasse aux immigrés, discours ultra sécuritaire, combat contre les syndicats ouvriers, anticommunisme virulent, ancrage catholique conservateur, conception de la place de la femme dans la société et, positionnement économique libéral.  En fait, le Front National a toujours été un parti de droite proposant des idées économiquement de droite défavorables aux classes populaires. Jean-Marie Le Pen était par exemple pour la retraite à 65 ans et pour la semaine de 42 heures. Il considérait Ronald Reagan comme un modèle alors qu'il fut certainement le président américain le plus libéral et le plus hostile aux travailleurs. Et aujourd’hui, malgré la peinture fraîche, rien n'a changé.  Le modèle politique de Marine Le Pen est aujourd'hui le richissime homme d'affaire Donald Trump et son gouvernement de multimilliardaires... Et cela se voit dans son programme avec un peu de décryptage. Quand Marine de Le Pen propose par exemple d’augmenter les salaires, elle ne propose en fait jamais d’augmenter le salaire brut, c’est à dire le salaire différé et socialisé. Avec le FN, on se paye soi même ses propres augmentations de salaires en transférant une partie de son salaire brut vers son salaire net. Quelle arnaque !!! Bref, les propositions du FN sont encore aujourd'hui d'inspiration profondément libérale et consistent toujours à faire baisser les « charges » des entreprises pour faire baisser le « coût du travail ». Parfois, ce positionnement très libéral est complètement assumé. A titre d'exemple, Bernard Monot, l'un des rédacteurs du programme économique du FN déclarait récemment : "Le FN est l'ami de toutes les entreprises, du petit commerçant au géant français du CAC40". Ou encore, "Je rappelle que nous sommes de vrais libéraux, partisans sans ambiguïté de l'économie de marché et la libre entreprise". Limpide...

Au final, malgré une rhétorique sociale, laïque et républicaine (qui sonne souvent faux), et avec la promotion d'un protectionnisme franco-francais dont le principal objectif est toujours le même depuis 40 ans (la fermeture des frontières aux étrangers), ses propositions restent immuablement dans la lignée de celles de son père. Avec Marine Le Pen, pas question d'augmenter réellement les salaires, le SMIC ne sera pas augmenté (ça serait une « mesurette » selon elle), les hauts revenus ne seront pas limités, il n'y aura pas de répartition des richesses, elle ne s'attaquera pas au grand capital. En refusant opiniâtrement de répartir la richesse entre capital et travail et en mettant la priorité sur le remboursement de la dette publique, elle propose finalement la même chose que ceux du « système » qu'elle critique tant.  Elle propose aux classes populaires de se serrer la ceinture, elle propose la rigueur et l’austérité dans un cadre national. Et puis regardez le FN en action. Aux dernières élections régionales, le FN a obtenu 350 élu-e-s . Qui peut citer une seule mesure de progrès social votée obtenue ou soutenue par ces élu-e-s régionaux ?

En définitive, ceux qui se présentent comme étant ni de gauche ni de droite, sont finalement bien de droite, et même assez banalement d’extrême droite. Dans son programme, Marine Le Pen ne propose en aucun cas de dé-financiariser l'économie ou de partager les richesses, les deux conditions nécessaires pour changer vraiment le sort des classes populaires. Vous voilà prévenus.

PS : je vous conseille cet excellent livre qui décrypte, comme son nom l'indique, l'imposture du FN et le danger des politiques libérales droitières. Bonne lecture :-)

![img-20170317-080052.jpg]