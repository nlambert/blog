---
title: Le 26 mai, votons Ian Brossat pour reconstruire la gauche
author: Nicolas Lambert
date: '2019-05-23'
slug: le-26-mai-votons-ian-brossat-pour-reconstruire-la-gauche
categories: []
tags:
  - Européennes
  - PCF
description: Article description.
featured: yes
toc: no
featureImage: 
thumbnail: images/brossat.jpg
shareImage: images/brossat.jpg
codeMaxLines: 10
codeLineNumbers: no
figurePositionShow: yes
---

**Il y deux ans à peine, la gauche était absente du second tour de l’élection présidentielle pour la deuxième fois sous les institutions de la cinquième République. On a trop banalisé cet événement. Il est temps de reconstruire une gauche digne de ce nom dans notre pays. Première étape : voter le 26 mai pour la liste conduite par Ian Brossat aux élections européennes.**

Le 7 mai 2017, il y deux ans à peine, la gauche était absente du second tour de l’élection présidentielle pour la deuxième fois sous les institutions de la cinquième République. On a trop banalisé cet événement majeur. Aujourd’hui la gauche est au tapis, éparpillée façon puzzle au point même que certains refusent de « s’enfermer dans ce mot qui n’a plus de sens ». Pourtant, quels que soient les trahisons et renoncements indéniables opérées sous le quinquennat Hollande - que le PCF a d’ailleurs combattu de bout en bout -, le clivage gauche-droite reste une grille de lecture pertinente pour notre société.

### C’est quoi la gauche ?

Les termes droite et gauche en politique remontent à la Révolution française. Le 28 août 1789, les députés étaient amenés à se prononcer sur le veto royal à la Constituante. Les députés opposés au veto du roi étaient invités à se regrouper à gauche du président du bureau, tandis que les partisans du veto royal devaient se placer à droite. L’origine même de la gauche est donc issu d’un processus révolutionnaire basé sur l’idée du pouvoir au peuple. L’idée de gauche, c’est la force du nombre contre un pouvoir dominant minoritaire.

Depuis, la vie politique française s’est construite autour de ce clivage. La gauche, c’est les congés payés avec le front populaire en 1936, la sécurité sociale en 1945 avec le programme du CNR et le ministre Ambroise Croizat, la réduction du temps de travail, la régularisation des sans papiers, les retraites, les services publics, etc. La gauche, c’est tous les conquis que les politiques de droite ne cessent d’essayer de rogner et défaire. Faire la retraite à 60 ans et défaire la retraite à 60 ans ce n’est quand même pas la même chose. Développer les services publics et réduire le nombre de fonctionnaires, c’est exactement le contraire. Réduire le temps de travail ou promouvoir le travailler plus pour gagner plus, c’est deux visions de la société. Et si certains partis classés à gauche comme le PS ou EELV sont traversés de part en part par le clivage gauche-droite, cela ne remet pas pour autant en cause l’essence même de ce clivage. Il est donc temps de reconstruire une gauche de combat, sincère et offensive. Car tout autre clivage qui viendrait se substituer à celui-là (nouveau monde vs ancien monde, mondialisme vs nationalisme, etc.) ne reviendra en réalité qu’à conduire petit à petit le Front National (RN) au pouvoir. Nous ne nous résoudrons jamais à une telle perspective.

### Reconstruire la gauche

Reconstruire la gauche après son effondrement sous le quinquennat Hollande est donc l'enjeu majeur des années à venir. Ca tombe bien, redonner des couleurs à la gauche au-delà de ces élections européennes, c’est justement le cap que s’est fixé le PCF, qui compte bien être un des moteurs de cette reconstruction. 


<blockquote class="twitter-tweet"><p lang="fr" dir="ltr">Je veux dire aux orphelins de la gauche que le PCF mettra sa force au service de la reconstruction de la gauche. <a href="https://twitter.com/IanBrossat?ref_src=twsrc%5Etfw">@IanBrossat</a> <a href="https://twitter.com/TV5MONDE?ref_src=twsrc%5Etfw">@TV5MONDE</a> <a href="https://t.co/T4gbCqedfi">pic.twitter.com/T4gbCqedfi</a></p>&mdash; PCF (@PCF) <a href="https://twitter.com/PCF/status/1131233850577833985?ref_src=twsrc%5Etfw">May 22, 2019</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>


Dans cette reconstruction nécessaire de la gauche, le PCF peut être un repère. Et cela pour plusieurs raisons :

### Une gauche de transformation sociale

A travers l’Histoire, le PCF a été présent dans tous les combats pour le progrès social. Aucune avancée sociale dans ce pays ne s’est faite sans le PCF. Aujourd’hui, le parti communiste propose un SMIC net à 1400 € et l’égalité salariale entre les femmes et les hommes. Il propose de prélever l’impôt à la source pour les profits multinationales, interdire les paradis fiscaux dans l’Union européenne et réorienter l’argent de la banque centrale vers les services publics, dé-financiariser l’économie en nationalisant une ou deux grandes banques françaises, sécuriser tous les parcours professionnels avec la SEF pour mettre fin au chômage et la précarité, donner aux salariés des droits nouveaux dans les entreprises, redéployer une véritable démocratie sociale avec une sécurité sociale pleine et entière, etc.. Nous ne résumerons pas ici l’ensemble des travaux et propositions du PCF. La littérature est abondante et détaillée dans tous ces domaines, je vous y renvoie. 

<blockquote class="twitter-tweet"><p lang="fr" dir="ltr">&quot;Êtes-vous pour un Smic européen ?&quot; ➡ on pose vos questions <a href="https://twitter.com/hashtag/ElectionsUE19?src=hash&amp;ref_src=twsrc%5Etfw">#ElectionsUE19</a> <a href="https://twitter.com/hashtag/VotreEurope?src=hash&amp;ref_src=twsrc%5Etfw">#VotreEurope</a> <a href="https://twitter.com/ChristianC2804?ref_src=twsrc%5Etfw">@ChristianC2804</a>, découvrez la réponse de <a href="https://twitter.com/IanBrossat?ref_src=twsrc%5Etfw">@IanBrossat</a> ⬇ <a href="https://t.co/D8KMrX9OdL">https://t.co/D8KMrX9OdL</a> <a href="https://t.co/pZpTfdCGba">pic.twitter.com/pZpTfdCGba</a></p>&mdash; franceinfo (@franceinfo) <a href="https://twitter.com/franceinfo/status/1131504301950480384?ref_src=twsrc%5Etfw">May 23, 2019</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

### Une gauche écolo

Loin des caricatures qu’on lui attribut souvent, le PCF est depuis longtemps converti à l’écologie. Et si certains ont en tête l’image d’un PCF productiviste (il faudrait d’ailleurs définir ce mot), c’est que le PCF a justement beaucoup travaillé sur la question de la production. Raison pour laquelle, il y inclut depuis longtemps la dimension écologique. Le communisme, c’est la mise en commun et le partage. C’est l’organisation collective. Comment ne pas voir, en effet, de lien avec les transports en communs ou le partage nécessaire des ressources naturelles limitées. D’ailleurs, Marx lui même avait déjà pointé du doigt, avant beaucoup d’autres, cette question. Les convergences sur ce point sont donc tout à fait évidentes et ont lieu d’ailleurs souvent dans la rue ou au parlement européen.

<blockquote class="twitter-tweet"><p lang="fr" dir="ltr">Il y a un consensus des scientifiques sur la réalité du réchauffement climatique, mais les lobbys se sont mobilisés pour empêcher les Etats d&#39;agir. Il faut sortir du tout camion en doublant le fret ferroviaire. <a href="https://twitter.com/hashtag/RedIsTheNewGreen?src=hash&amp;ref_src=twsrc%5Etfw">#RedIsTheNewGreen</a> <a href="https://twitter.com/hashtag/LaGrandeConfrontation?src=hash&amp;ref_src=twsrc%5Etfw">#LaGrandeConfrontation</a> <a href="https://twitter.com/hashtag/AvecBrossat?src=hash&amp;ref_src=twsrc%5Etfw">#AvecBrossat</a> <a href="https://t.co/5AxP3KchnJ">pic.twitter.com/5AxP3KchnJ</a></p>&mdash; PCF (@PCF) <a href="https://twitter.com/PCF/status/1130524661463298056?ref_src=twsrc%5Etfw">May 20, 2019</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

<blockquote class="twitter-tweet"><p lang="fr" dir="ltr">Sur le glyphosate que nos eurodéputés veulent interdire immédiatement, Macron s&#39;est comporté comme un disciple des lobbys <a href="https://twitter.com/IanBrossat?ref_src=twsrc%5Etfw">@IanBrossat</a> <a href="https://twitter.com/hashtag/LaGrandeConfrontation?src=hash&amp;ref_src=twsrc%5Etfw">#LaGrandeConfrontation</a> <a href="https://twitter.com/hashtag/AvecBrossat?src=hash&amp;ref_src=twsrc%5Etfw">#AvecBrossat</a> <a href="https://t.co/jTwjr1OLzJ">pic.twitter.com/jTwjr1OLzJ</a></p>&mdash; PCF (@PCF) <a href="https://twitter.com/PCF/status/1130522276766593024?ref_src=twsrc%5Etfw">May 20, 2019</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

### Une gauche sincère

Le PCF c’est des milliers d’hommes et de femmes qui se battent au quotidien, non pas pour leur pomme individuelle, mais pour les intérêts collectifs des classes populaires. Le PCF c’est une force militante énorme. Nous sommes présents partout sur le territoire. Et si on considère le montant de la cotisation des adhérents, c’est même le premier parti de France [voir](https://www.legifrance.gouv.fr/jorf/id/JORFTEXT000037972463)

<blockquote class="twitter-tweet"><p lang="fr" dir="ltr">Et quand 2000 personnes entonnent l’Internationale à pleins poumons à la fin du meeting de Ian Brossat... ❤️✊ <a href="https://t.co/rKBmlQUgXf">pic.twitter.com/rKBmlQUgXf</a></p>&mdash; En Marx ! (@TeamEnMarx) <a href="https://twitter.com/TeamEnMarx/status/1129305991030480897?ref_src=twsrc%5Etfw">May 17, 2019</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

De plus, le PCF c’est aussi des milliers d’élus pour lesquels une règle simple s’applique. Cette règle, définie dans nos statuts, stipule qu’au PCF, personne ne s’enrichit avec la politique. Tout élu reverse au parti la part de ses indemnités qui lui procurerait une augmentation de revenu par rapport à son salaire avant d’être élu. Cette règle vertueuse qu’aucun autre parti ou organisation ne met en œuvre, s’applique à tous, y compris à Ian Brossat. Voter PCF le 26 mai, c’est la certitude de voter pour des gens qui se battent par conviction et non pour le montant de leurs indemnités. Tout le monde ne peut pas en dire autant [voir](https://www.liberation.fr/checknews/2019/05/01/ian-brossat-remet-il-une-partie-de-ses-indemnites-d-elu-au-parti-communiste_1724156)

### Une gauche cohérente

Le PCF c’est aussi une gauche cohérente. C’est une gauche qui s’est opposée à tous les traités européens libéraux à commencer par celui de Maastricht. Nous n’avons pas changé d’avis en cours de route. Et comme l’a dit Ian Brossat mercredi sur France 2, « Quand je vois que ceux qui ont voté tous les traités ultralibéraux, de Maastricht au TCE, dire qu'ils sont contre la concurrence libre et non faussée, ce sont des faux-culs, ce sont des hypocrites. Nous, nous avons rejeté tous ces traités. ».

<blockquote class="twitter-tweet"><p lang="fr" dir="ltr">Tête de liste PCF, Ian Brossat dénonce la position de certains candidats aux <a href="https://twitter.com/hashtag/Europ%C3%A9ennes2019?src=hash&amp;ref_src=twsrc%5Etfw">#Européennes2019</a> sur la &quot;concurrence libre et non faussée&quot;. &quot;Ce sont des faux-culs, ce sont des hypocrites&quot;, ajoute-t-il. <a href="https://t.co/ogvZufoArW">pic.twitter.com/ogvZufoArW</a></p>&mdash; Vous avez la parole (@VALP) <a href="https://twitter.com/VALP/status/1131311206264901638?ref_src=twsrc%5Etfw">May 22, 2019</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

Sur cette question de la cohérence, cette interview de Francis Wurtz (ancien député européen PCF) de 1992, au moment du traité de Maastricht est tout à fait éclairante. Avions-nous tort ? Ou raison ?

<blockquote class="twitter-tweet"><p lang="fr" dir="ltr">🎥 En avril 1992, <a href="https://twitter.com/hashtag/FrancisWurtz?src=hash&amp;ref_src=twsrc%5Etfw">#FrancisWurtz</a>, député européen du <a href="https://twitter.com/PCF?ref_src=twsrc%5Etfw">@PCF</a>, expose les raisons pour lesquelles les communistes s&#39;opposent au traité de <a href="https://twitter.com/hashtag/Maastricht?src=hash&amp;ref_src=twsrc%5Etfw">#Maastricht</a> et au tournant ultralibéral qu&#39;il impose à la construction européenne. 🇪🇺<a href="https://t.co/QNbQYBJ9Nr">https://t.co/QNbQYBJ9Nr</a> <a href="https://twitter.com/hashtag/ElectionsEuropeennes?src=hash&amp;ref_src=twsrc%5Etfw">#ElectionsEuropeennes</a> <a href="https://t.co/a6LzyEFLD8">pic.twitter.com/a6LzyEFLD8</a></p>&mdash; Ciné-Archives (@CineArchives) <a href="https://twitter.com/CineArchives/status/1130462175804428289?ref_src=twsrc%5Etfw">May 20, 2019</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

### Une gauche populaire

Dans notre pays, les ouvriers représentent encore 20 % de la population. Il y a plus d’ouvriers de que cadres en France. Pourtant, ceux-ci sont éjectés des grandes villes, où ils n’ont pas les moyens de se loger, et sont invisibles dans les médias et les institutions. Tout comme d’ailleurs les habitants des quartiers populaires, souvent issus de l’immigration. C’est autour de ces invisibles, de ces déclassés, de ces oubliés que nous devons reconstruire la gauche. Car la gauche c’est çà. C’est la voix des classes populaires qui aujourd’hui, ne vont plus voter. Cela, le PCF l’a bien compris. En plaçant Marie-Hélène Bourlard (ouvrière textile du Nord) en 2e position sur sa liste aux européennes, le parti propose pas moins que de faire élire la première femme ouvrière française au parlement européen depuis 30 ans. Ca vaut le coup de se battre.

<blockquote class="twitter-tweet"><p lang="fr" dir="ltr">Marie-Hélène Bourlard est ouvrière dans le textile. Si elle est élue elle sera la première femme ouvrière au Parlement européen depuis 30 ans. Elle peut être élue : avec 5% on fait élire 5 députés de gauche ! <a href="https://twitter.com/hashtag/LaGrandeConfrontation?src=hash&amp;ref_src=twsrc%5Etfw">#LaGrandeConfrontation</a> <a href="https://twitter.com/IanBrossat?ref_src=twsrc%5Etfw">@IanBrossat</a> <a href="https://twitter.com/hashtag/AvecBrossat?src=hash&amp;ref_src=twsrc%5Etfw">#AvecBrossat</a> <a href="https://t.co/HBpEvRRx9s">pic.twitter.com/HBpEvRRx9s</a></p>&mdash; PCF (@PCF) <a href="https://twitter.com/PCF/status/1130535894459518981?ref_src=twsrc%5Etfw">May 20, 2019</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

### Une gauche utile et unitaire

On reproche souvent aux communistes ses alliances aux élections locales et notamment avec le PS. Vu ce qui a été dit plus haut sur le quinquennat Hollande, cette critique peut s’entendre. Pourtant, force est de constater que les communistes élus locaux font du bon boulot. Cette envie d’être utile les poussent à agir concrètement pour les classes populaires. Développer des services publics, des solidarités locales, de l’alimentation bio, la culture, etc. Développer des logements sociaux aussi, comme le fait Ian Brossat à Paris. Plus de 100 000 logements sociaux ont été construits à Paris depuis 2001. Sacré bilan. Nos élus ne chôment pas.

<blockquote class="twitter-tweet"><p lang="fr" dir="ltr">Omar Sy, Shy’m, Jamel Debbouze, Sophia Aram… Quelques unes des plus grandes stars françaises viennent de Trappes, et c’est grâce à la politique du Parti communiste selon la journaliste Ariane Chemin.<a href="https://twitter.com/hashtag/Quotidien?src=hash&amp;ref_src=twsrc%5Etfw">#Quotidien</a> <a href="https://t.co/8AyVKSvyht">pic.twitter.com/8AyVKSvyht</a></p>&mdash; Quotidien (@Qofficiel) <a href="https://twitter.com/Qofficiel/status/953708747704143872?ref_src=twsrc%5Etfw">January 17, 2018</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

### Dimanche 26 mai, le bulletin de la reconquête à gauche, c’est le bulletin Ian Brossat ! 

Voter Ian Brossat le 26 mai, c’est la première pierre de cette reconquête. C’est la première étape pour redonner des couleurs à la gauche. Et dès le lendemain des élections, nous tendrons la main aux autres forces comme nous l’avons toujours fait. Sans compromission. Avec détermination. Sans céder sur nos combats et nos valeurs. Car tendre la main, c’est toujours ce qui a caractérisé le PCF, y compris quand il était au somment de sa puissance. Et c’est justement grâce à cet esprit unitaire que nous avons pu construire les avancées sociales dans notre pays, que la droite détruit systématiquement dès qu’elle est au pouvoir, malgré les résistances nombreuses des travailleurs.

Il s'agit maintenant de relever la tête, de repartir à l'offensive et reprendre ce qui nous a été volé. Reconstruire une gauche de combat pour remettre en marche le chemin du progrès social. Dans ce combat, vous pourrez compter sur nous. PCF is back ! 

<blockquote class="twitter-tweet"><p lang="fr" dir="ltr">▶️Simulation : <a href="https://twitter.com/IanBrossat?ref_src=twsrc%5Etfw">@IanBrossat</a> à 4% ou <a href="https://twitter.com/IanBrossat?ref_src=twsrc%5Etfw">@IanBrossat</a> à 5%, qu&#39;est ce que ca change pour la gauche ?<br>▶️Réponse : le rapport de force de l’échiquier politique sera considérablement différent.<br>▶️Conclusion : chaque voix compte et ca vaut le coup de se déplacer.<a href="https://t.co/XdC9F7cTR5">https://t.co/XdC9F7cTR5</a></p>&mdash; Cartographe encarté (@nico_lambert) <a href="https://twitter.com/nico_lambert/status/1131546293262835713?ref_src=twsrc%5Etfw">May 23, 2019</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>