---
title: Gonfler les biceps
author: Nicolas Lambert
date: '2018-02-01'
slug: gonfler-les-biceps
categories: []
tags:
  - Syriza
description: Article description.
featured: yes
toc: no
featureImage: 
thumbnail: images/tsipras-melenchon-laurent-grexit.jpg
shareImage: images/tsipras-melenchon-laurent-grexit.jpg
codeMaxLines: 10
codeLineNumbers: no
figurePositionShow: yes
---

**Ma réaction au communiqué du PG demandant l’exclusion de Syriza du PGE.**

Dans un [communiqué](https://www.lepartidegauche.fr/le-parti-de-gauche-sadresse-au-pge/), le Parti de Gauche appelle à l’exclusion de Syriza du PGE (parti de la gauche européenne). Bon...

On le sait, l’élection d’Alexis Tsipras a soulevé beaucoup d’espoir. Le gouvernement Syriza aurait effectivement pu être le premier gouvernement anti austérité en Europe et un point d’appui important dans différents pays. Mais, malgré l’appui massif du peuple grec dans la rue et dans les urnes, Tsipras a fini par céder à un rapport de force très défavorable avec la commission UE, le FMI, les autres gouvernements européens et les puissances capitalistes privées. Une sorte de tournant de la rigueur à peine élu. Un sacré gâchis. La politique d’austérité menée par Tsipras dans son pays est de fait inacceptable. Et il va de soi que si j’étais Grec, je manifesterai dans la rue contre les mesures prises par ce gouvernement. Aucun doute la dessus.

Maintenant, ce qu’il faut comprendre aussi c’est que si il y avait en Europe un rapport de force différent et une majorité successible de mener une politique de progrès social et écologique, Tsipras n’en serait pas un opposant mais probablement un des protagonistes. Du coup, je me demande vraiment à quoi rime ce genre d’injonction visant à demander l’exclusion de Syriza du PGE. Ca réglerait quoi ? Honnêtement, je vois surtout dans ce communiqué un moyen de faire du buzz et de la com’ sachant que ça ne produira aucun effet concret. C’est juste une façon de gonfler les biceps pour dire “eux ce sont des traîtres mais nous on tiendra bon. Et tous ceux qui ne veulent pas exclure Tsipras sont des traîtres aussi”. Franchement, c’est un peu court et pas vraiment constructif. Et finalement, cette façon de faire de la politique spectacle a coup d’effet de manche commence sérieusement à me fatiguer...

NB : Pour mémoire, on peut rappeler aussi que le PG avait déjà suspendu sa participation au PGE en 2013 à cause de... l'élection de Pierre Laurent à la tête du groupe. Hum hum...

--------------------------------------------

ADDENDUM : 

Ci-dessous, je reproduis l'analyse de Maxime Cochard (PCF) que je partage pleinement.

"La critique du gouvernement Syriza, pourquoi pas, mais encore faut-il connaître un minimum la Grèce et la situation politique. Ceux qui mettent l'hyper-austérité sur le compte de Tsipras "le traître" ne sont pas sérieux. La Grèce est un pays périphérique à Etat faible, satellisé par les puissances impérialistes et doté d'une bourgeoisie comprador. Alors ceux qui croient qu'il "suffirait" de sortir de l'euro, que "yaka" décréter une politique de gauche, comment dire...

Ces camarades semblent persuadés que notre soutien (relatif et contradictoire) à Syriza nous "coûte" des millions de voix. Or en France tout le monde s'en fout. Ça ne nous retire pas plus de voix que ça nous en rapporte. En faire un point dur de nos débats internes n'a aucun sens.

Pendant ce temps toute la galaxie FI se met à jouer la même mélodie : Tsipras salaud ! Traître ! Macronien ! Et "Le Média" interroge Zoé Konstantopoulou, ancienne présidente du Parlement grec et alliée de Mélenchon (qui fera la campagne européenne avec lui), qui déclare que le Gouvernement Tsipras fait pire que la droite.

La droite de Samaras a maquillé les comptes, bradé le pays à ses amis, couvert les assassinats des nazis. Syriza, certes contraint d'appliquer les abjects mémorandums de Schaüble à cause d'un chantage permanent à l'aide financière, a tout de même réussi à supprimer les franchises médicales, créer des allocations pour les retraités pauvres, régulariser des centaines de milliers de sans-papiers et ouvrir des droits sociétaux...

De fait, Zoé K. ne pèse absolument rien dans l'opinion grecque, et son groupuscule ("Trajet de liberté") n'est même pas testé dans les sondages. La scission "de gauche" de Syriza, qui revendique la sortie de l'euro, est donnée à 1% (sondage Pulse du 29/01, où Syriza est à 24%).

Sur la question de la remise en cause du droit de grève, nous sommes dans le cadre de l'éternel chantage des créanciers vis-à-vis des débiteurs, avec en plus le côté "troll" des créanciers. Il s'agit d'humilier le Gouvernement en échange de la tranche d'aide indispensable à l'économie grecque. C'est déplorable, mais ce ne sont pas nos camarades les coupables.

Personnellement je me satisfais de la réaction très ferme du PCF qui renvoie Coquerel dans ses cordes sans ménagement. Ça fait d'ailleurs bien longtemps que nous aurions eu besoin d'une telle clarté dans nos relations avec la FI dans d'autres domaines."