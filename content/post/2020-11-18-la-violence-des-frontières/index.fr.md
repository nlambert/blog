---
title: La violence des frontières
author: Nicolas Lambert
date: '2019-11-18'
slug: la-violence-des-frontières
categories: []
tags: []
description: Article description.
featured: yes
toc: no
featureImage: 
thumbnail: images/noborders.png
shareImage: images/noborders.png
codeMaxLines: 10
codeLineNumbers: no
figurePositionShow: yes
---

**Seuls 10 % des enfants grandissent dans un pays riche. Une inégalité à la naissance qui devrait nous conduire collectivement à défendre la liberté de circulation comme le proclame l’article 13 de la déclaration universelle des droits de l’Homme. Au lieu de ça, les états ne cessent d’ériger des barrières entre les humains : murs, camps, barbelés, etc. Un régime global et mortifère des frontières.**

Le 22 novembre 2019, je participais au Before organisé par la maison des métallos. Au programme, la projection du film « Middle of the moment » de Nicolas Humbert et Werner Penzel et un échange croisé sur la mobilité, le nomadisme et les géographies migratoires. Je résume ici le contenu de mon intervention.


### 7 milliards d’humains

Avant de parler de migrants, je voudrais rappeler une chose évidente qui peut paraître banale ici mais par laquelle je souhaite commencer : nous sommes tous des êtres humains. Nous sommes aujourd’hui 7 milliards d’humains sur la planète. Parmi nous, 49% sont des femmes, 51 % sont des hommes. 53% sont des enfants (soit 3,9 milliards). Un enfant sur deux vit en Asie. Un enfant sur quatre vit en Afrique. Seuls 10% des enfants dans le monde grandissent dans un pays riche.

Une caractéristique du Monde dans lequel nous vivons est donc d’être inégalitaire. Il est discontinu et cloisonné par des frontières. Je le redis, tout le monde n’a pas la chance de naître dans un pays riche. Aujourd’hui dans le monde, 800 millions de personnes sont sous-alimentées, soit une personne sur dix qui souffre de la faim. Nous avons donc sous les yeux un monde découpé façon puzzle. Des pays riches. Et des pays pauvres. Et entre les deux, des frontières plus ou moins violentes sur lesquelles se cristallisent des tensions migratoires. Mais ce n’est pas aussi simple. Je profite donc de cette occasion pour tailler en brèches quelques idées reçues.

##### Idée reçue n°1 : Il y a de plus en plus de migrations

Au total, l’ONU estime le nombre des migrants internationaux en 2019 à 272 millions, soit 50 millions de plus qu’en 2010. En proportion, cela concerne 3,5% de la population mondiale. Ce chiffre est en légère augmentation mais reste relativement stable dans le temps. En 1990, le nombre de migrants internationaux s’élevait à 152 millions, soit 2,9 % de la population mondiale. Mais en 1913, à la veille de la première guerre mondiale, il représentait 5% de la population du globe. En réalité, les migrations internationales ne sont pas nouvelles et font parti intégrante de ce qu’est l’humanité en tant que telle. L’homme préhistorique est sorti d’Afrique en migrant. Et plus récemment, rappelons qu’au 19e siècle, ce sont 50 millions d’européens ont migré vers les Amériques. Cela représentait alors près de 12% de la population européenne. Un véritable exode.

##### Idée reçue n°2 : les pauvres migrent vers les pays riches

Autre représentation qu’on a en tête, les migrants seraient des hommes pauvres venus du sud qui se dirigeraient inéluctablement vers les riches pays du Nord. En réalité, seuls 37 % des migrations dans le monde ont lieu d’un pays en développement vers un pays développé. La majorité des migrations ont lieu entre les pays du Sud. Et seuls 20 % des migrants africains se dirigent vers l’Europe. Avec notre vision franco et européo-centrée, on oublierait presque que l'Asie est le continent d'où provient le plus grand nombre de migrants (106 millions) et que l'Inde en fournit à elle seule 17 millions. Les États-Unis sont le pays qui accueille le plus de migrants, avec près de 50 millions de personnes, tandis que l'Arabie saoudite, l'Allemagne et la Russie en accueillent chacun environ 12 millions. Et puis n’oublions pas que les migrations ne sont pas des flèches rectilignes unidirectionnelles mais des circulations. Il y a les allers et il y a les retours. Or on parle souvent de ceux qui arrivent mais jamais de ceux qui partent. On ne parle jamais, par exemple, des 3 millions de français qui vivent à l’étranger. Mais ceux-là, on ne les appelle pas des migrants, on les appelle des expatriés. Ou bien exilés fiscaux quand ils ont la chance d’avoir un compte en banque bien garni...

##### Idée reçue n°3: Il y a une crise migratoire

Je crois qu’il faut tailler en brèche cette idée selon laquelle il y aurait eu, en 2015, une crise migratoire en Europe. En réalité, au regard des mouvements de population qu’il y a eu à ce moment là, force est de constater que ce qu’à vécu l’Europe est plutôt une crise de l’accueil. Prenons l’exemple des réfugiés syriens qui ont fuit la guerre et Daech. Certes, l’Europe en a accueilli un certain nombre. Certains pays ont d’ailleurs été plus accueillants que d’autres (L’Allemagne, la Suède, Malte, Chypre). Mais si on décentre un peu le regard, on se rend aussitôt compte que ce sont les pays limitrophes de la Syrie qui ont réellement accueilli. Turquie, Liban, Jordanie, Arabie Saoudite. Et si on ajoute à cela les déplacés internes (c’est à dire ceux qui ont migrent à l’intérieur de la Syrie, sans franchir une frontière), on se rend aussitôt compte que l’Europe n’a pas été du tout à la hauteur des enjeux. Et cela est une règle générale, dans l’urgence, les réfugiées se mettent à l’abri au premier endroit où cela est possible et non vers un riche pays lointain.

##### Idée reçue n°4: Les migrants sont des jeunes hommes

Qui sont les migrants ? La encore, beaucoup d’idées reçues. Il peut s’agir de jeunes hommes, diplômés, séduits par la modernité occidentale. Ou alors des hommes peu qualifiés qui n’ont d’autre choix que de migrer pour voir leurs conditions de vie s’améliorer. Mais il s’agit aussi bien souvent de femmes, isolées, instruites, ou qui aspirent à l’indépendance et qui sont éprises de liberté. Car ce qu’on ne dit jamais assez, c’est que près de la moitié des migrants sont des migrantes. On trouve également beaucoup d'enfants mineurs. Au total, ceux qui migrent ne sont généralement pas les plus pauvres. Ce sont ceux qui disposent d’un capital spatial, financier et culturel qui leur permet de traverser les frontières. Les causes des migrations sont diverses aussi. Elles sont difficiles à démêler. Il s’agit souvent de facteurs combinés : le néolibéralisme ; le réchauffement climatique ; la faim ; la nécessité de quitter un mari violent ; le besoin de se rapprocher de sa famille ; l’espoir d’un avenir meilleur ; l’envie de démocratie ; travailler ; faire face à un système social déstabilisé ; étudier ; survivre, etc. Souvent, au cours de leur périple, des gens aux histoires différentes cheminent ensemble pendant des années. Faut il vraiment les trier à l’arrivée ? Faut-il renvoyer les « mauvais migrants » chez eux ? D’ailleurs, ou est-ce « chez soi » quand on a tout quitté et que cela fait des années qu’on est sur les routes ?

### Le durcissement des frontières

Face à cette nécessité impérieuse de franchir les frontières, la réponse des états est le plus souvent sécuritaire. Et même si plusieurs espaces de libre circulation existent à travers le monde, ils se construisent souvent en rejetant ceux qui n’en font pas parti. L’espace Schengen illustre bien cela. C’est à la fois un espace de libre circulation (construit avant tout sur l’idée du libre échange économique) et qui pour cela, durcit de manière drastique ses frontières externes, avec la mise en place de dispositifs militaires et un renforcement progressif des législations (durée d’enfermement, expulsions, fichage biométrique, etc. ). A la fois l’ouverture et la fermeture. La médaille et son revers.

##### Des murs

Un exemple visible de ce durcissement des frontières dans le monde, c’est l’érection de murs. On a beaucoup parlé ces dernières semaines de la chute du mur de Berlin, tombé il y a 30 ans tout juste. Tout le monde s’accorde pour dire que c’est une bonne chose que ce mur soit tombé. Or ce que peu de gens savent, c’est qu’on comptabilise aujourd’hui 40 000 km de murs et barrières dans le monde aux frontières des états, soit l’équivalent de la circonférence terrestre. Mesurons bien cela. Alors même que nous vantons les louanges d’un monde interconnecté et globalisé, il n’y a jamais eu autant de murs pour séparer les Hommes. C’est un fait : la mondialisation cloisonne aussi.

##### Des camps

Autre dispositif destiné à empêcher la mobilité des humains : l’enfermement. Cette idée n’est pas nouvelle. En Europe, les premiers lieux d'enfermement pour étrangers sont apparus dans les années 1960. Puis, dans les années 1990, leur nombre a commencé à croître considérablement. Mais ce n’est que depuis 2003, que les centres de rétention sont identifiés comme un outil central de la politique migratoire en Europe. De 2011 à 2016, la capacité totale connue des camps recensée par le réseau Migreurop est passée de 32 000 à 47 000 places. On estime également que 600 000 à 700 000 étrangers seraient maintenus en rétention chaque année. En France, depuis la loi Asile immigration, la durée maximale d’enfermement peut aller jusqu’à 90 jours. Ailleurs en Europe, cela peut aller jusqu’à 18 mois ! Et tout cela pour des personnes qui n’ont commis ni crime ni délit. D’ailleurs, les enfants aussi sont enfermés. En France en 2017, il y avait 304 enfants enfermés alors que leur nombre était de 40 en 2013, et avec une durée moyenne qui est passée de 2011 à 2016 de 8,7 à 12, 2 jours.

L’enfermement des migrants a lieu aujourd’hui bien au delà de l’espace européen. La logique est la suivante : plus on arrive à stopper les migrants en amont, mieux c’est pour l’Europe. Ce qui complique drastiquement la vie des migrants. La frontière n’est en réalité plus une simple ligne sur une carte, mais un réseau tissé sur les pays du voisinage et sur les routes migratoires.

##### Des morts

Une des conséquence directe de toutes ces entraves systématiques à la mobilité, c’est que pour avoir une chance de passer, il faut emprunter des routes toujours plus dangereuses et mettre sa vie entre les mains de mafias peu scrupuleuses. Au delà de toutes les violences que rencontrent les migrants (physiques, morales, sexuelles), certaines sont létales. On estime qu’il y aurait plus de 50 000 personnes mortes ou portées disparues depuis le début des années 90 aux frontières de l’Europe, ce qui en fait de loin de loin la frontière la plus mortifère au monde [voir]. Et cela est une conséquence directe des politiques migratoires assassines que mènent les pays européens. Et n’oublions pas qu’en France aussi, des hommes et des femmes meurent en migration, souvent dans l’indifférence générale et de façon anonyme, comme c’est le cas à Calais.

##### Deux mondes face à face

Au final, ce que nous montrent les cartes, c’est l’image d’un « régime global des frontières destiné à capter les ressources et empêcher la liberté de circulation » (Reece Jones, 2016). On a d’un côté des riches qui peuvent circuler librement et de façon sécurisée. Et de l’autre, des pauvres qui pour pouvoir vivre leur vie, sont contraints à emprunter des routes toujours plus dangereuses et plus violentes, plus longues et plus coûteuses, au péril de leur vie. 
