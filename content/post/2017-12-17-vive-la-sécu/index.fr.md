---
title: Vive la sécu
author: Nicolas Lambert
date: '2017-12-17'
slug: vive-la-sécu
categories: []
tags:
  - sécu
  - tract
description: Article description.
featured: yes
toc: no
featureImage: 
thumbnail: images/croisat-p1.png
shareImage: images/croisat-p1.png
codeMaxLines: 10
codeLineNumbers: no
figurePositionShow: yes
---

**Aujourd’hui, la sécurité sociale est menacée de disparition : suppressions des cotisations sociales, remplacement par la CSG, développement des exonérations patronales, accroissement du forfait hospitalier, multiplication des non remboursements des frais médicaux, etc. Ne laissons pas Macron remettre en cause nos conquêtes sociales.**

![images/croisat-p1.png]

![images/croisat-p2.png]

### Qui est Ambroise Croizat ?

Né en 1901, d’un père manœuvre et d’une employée dans un tissage de velours, il travaille en usine dès l’age de 13 ans quand son père doit partir à la guerre en 1914. Apprenti métallurgiste, l’enfant suit les cours du soir et devient ajusteur-outilleur. Dès 1914, il s’engage dans l’action syndicale à la CGT (dont il deviendra le secrétaire général en 1927) et adhère au PCF en 1920. Élu député de Paris sous le front populaire, il se battra pour les congés payés, la semaine de 40 heures et la loi sur les conventions collectives. Arrêté pour ses opinions politiques en 1939, il sera incarcéré, fers aux pieds, jusqu’en 1943. Il se met alors à travailler dans la clandestinité avec d’autres résistants, au programme du CNR et à ce qui deviendra la future sécurité sociale. Le 13 novembre 1945, Ambroise Croisat est nommé ministre du travail. 138 caisses de sécurité sociales sont édifiées en seulement 6 mois. La sécu est née. Ambroise Croizat meurt à Paris le 10 février 1951. Ils étaient un million pour l’accompagner au Père Lachaise.

### Le salaire socialisé, c’est quoi ?

Un employeur fait deux versements. L’un directement au salarié, c’est le salaire net qu’on touche tous les mois. L’autre, sous forme de cotisations sociales, c’est la composante collective (ou salaire socialisé). La cotisation sociale fait donc partie intégrante du salaire et permet de financer la sécurité sociale. Quand Emmanuel Macron propose accroître le pouvoir d’achat en diminuant les cotisations sociales, il ne propose rien d’autre que de le financer avec votre propre argent, tout en détruisant la solidarité entre les travailleurs. Attention danger !

*« La sécurité sociale est la seule création de richesse sans capital. La seule qui ne va pas dans la poche des actionnaires mais est directement investie pour le bien-être de nos citoyens »*

### Les 4 grands principes sur lesquels s'est fondée la sécu

- Unicité : Tous les “risques sociaux” (maladie, maternité, vieillesse, accidents du travail, ...) sont regroupés dans une seule caisse.

- Universalité : Toute la population a droit à une protection sociale tout au long de la vie, les enfants de riches, comme les enfants de pauvres.

- Solidarité : Un système de répartition entre actifs et non actifs financé par les richesses crées dans l’entreprise.

- Démocratie : Une gestion de l’institution par les bénéficiaires eux-mêmes.

### En 2018, nous proposons

- Santé : Reconquérir la généralisation de la prise en charge à 100 % pour tous des soins en commençant par la prise en charge immédiate à 80 % pour tous et à 100 % pour les jeunes de moins de 25 ans.

- Retraites : Garantir le droit à la retraite à 60 ans à taux plein.

- Travail : Renforcer la médecine du travail en liaison avec les CHSCT pour améliorer les conditions de travail de tous.  

- Famille : Étendre les prestations familiales par la création d’une allocation pour le premier enfant pour toutes les familles et revalorisation des prestations pour le deuxième enfant.

- Démocratie : Rétablir l’élection au suffrage universel des représentants des assurés sociaux dans les conseils d’administration des caisses de sécurité sociale.

- Éradiquer le chômage : Protéger les travailleurs tout au long de leur carrière professionnelle à travers une sécurisation de l’emploi et de la formation en assurant une continuité des droits sociaux et des revenus tout au long de vie.

### Comment financer ?

ll faut dire la vérité : ce qui pèse aujourd’hui sur la vie quotidienne de la population, c’est le coût du capital ! En 2011, les dividendes et intérêts des entreprises totalisaient 309 milliards d’euros. Et des milliards s’envolent chaque année en fumée dans les paradis fiscaux. Pour financer durablement la sécurité sociale, nous devons la libérer du carcan imposé par son étatisation et de toute forme de fiscalisation (CSG, TVA “sociale”, ...). Seul le travail est producteur de richesses. Un point de plus sur la masse salariale, c’est 2 milliards de recettes en plus pour la Sécurité sociale. 100 000 chômeurs en moins, c’est 2,5 milliards ! Augmenter les salaire des femmes (qui gagnent 23 % de moins que les hommes), c’est autant de cotisations en plus dans les caisses. En résumé, pour financer la sécu, sortons des politiques d’austérité !

----

*« Jamais, nous ne tolérerons que soit renié un seul des avantages de la Sécurité Sociale. Nous défendrons à en mourir, et avec la dernière énergie, cette loi humaine et de progrès » (Ambroise Croizat, octobre 1950)*


