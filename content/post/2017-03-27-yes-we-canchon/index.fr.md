---
title: Yes we canchon !
author: Nicolas Lambert
date: '2017-03-27'
slug: yes-we-canchon
categories: []
tags:
  - Mélenchon
  - Présidentielle
description: Article description.
featured: yes
toc: no
featureImage: 
thumbnail: images/union2.png
shareImage: images/union2.png
codeMaxLines: 10
codeLineNumbers: no
figurePositionShow: yes
---

**A 30 jours du premier tour, l'unité de la gauche de transformation sociale, n'est malheureusement plus une option crédible. Donc face à ce paysage quasi certain - deux candidatures de gauche anti Hollande - j'invite chacun à se positionner clairement. Pour moi, c'est clair, c'est Mélenchon !**

Aujourd'hui, au lendemain du premier débat sur TF1, une semaine après la grande marche parisienne pour la sixième république, et tandis que certains continuent à pousser aussi loin qu'ils le peuvent l'idée d'une candidature commune à l’élection présidentielle basée sur un pacte de majorité, soyons réalistes : l'unité de la gauche de transformation sociale, à 30 jours du premier tour, n'est plus une option crédible. Même si cette stratégie de rassemblement était probablement la seule stratégie possible pour avoir une chance d’accéder au second tour, on ne fait pas dévier aussi facilement un train lancé à toute allure. Qu'il s'agisse du frondeur Hamon ou de l'insoumis Mélenchon, chacun est dans son couloir et avance en s'appuyant sur des légitimités et des forces qu'il est difficile de contester. Donc face à ce paysage quasi certain - deux candidatures de gauche anti Hollande - j'invite chacun à se positionner clairement. 

![images/union2.png]

En ce qui me concerne, les choses sont limpides et elles l'ont toujours été. Je considère qu'il faut défendre bec et ongles la candidature de Jean-Luc Mélechon à l’élection présidentielle et cela pour deux raisons : 

### Première raison : le quinquennat Hollande

Après un quinquennat tourné contre les travailleurs et même si Benoit Hamon s'inscrit en rupture vis à vis du quinquennat, le PS n'est pas en mesure de rassembler le peuple de gauche. Loi travail, ANI, CICE, loi travail, loi Macron, pacte budgétaire, etc., la rupture est profonde. Et beaucoup d'entre nous ont encore dans la bouche le gout acre des gaz lacrymogènes et de l'intransigence de Bernard Cazeneuve qui soutient aujourd'hui Benoit Hamon. Le slogan "Plus jamais PS" lancé par le nuitedboutsite François Ruffin raisonne encore chez beaucoup d'entre nous.

### Deuxième raison : le programme

Même si ce programme ne me convient pas à 100%, c'est évidemment aujourd'hui celui, parmi les candidats à la présidentielle, le plus proche des valeurs que je défends. Ce programme est réaliste, cohérent et chiffré. C'est un point d'appui solide qui ne nous empêche en rien de défendre aussi nos propres propositions et notamment la loi sécurité emploi formation (SEF).  Il s'agit ici d'une réelle possibilité de remettre enfin en marche le chemin du progrès social et écologique.

![union.png]

### Alors hauts les cœurs !

Élargissons le cadre de rassemblement, travaillons collectivement et faisons tout pour propulser Jean-Luc Mélenchon à la tête du pays. Pour cela, toutes celles et ceux qui ont envie d'aller dans ce sens doivent se mettre en mouvement et avancer ensemble dans le respect de chacun. Si nous réussissons à nous rassembler sur le terrain, à créer du collectif impliquant toutes les forces militantes à la gauche du PS, nous réussirons à fédérer le peuple et construire une force populaire capable de créer la surprise le 23 avril. La dynamique est là, alors appuyons sur l’accélérateur. Vroum !!