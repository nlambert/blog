---
title: 'Tous unis contre Macron '
author: Nicolas Lambert
date: '2018-05-03'
slug: tous-unis-contre-macron
categories: []
tags: []
description: Article description.
featured: yes
toc: no
featureImage: 
thumbnail: images/dscn7359.jpg
shareImage: images/dscn7359.jpg
codeMaxLines: 10
codeLineNumbers: no
figurePositionShow: yes
---

**Hier sur France Inter, le secrétaire général de la CGT, Philippe Martinez, énonçait la vérité suivante : “évidement on a des différences. Mais dans l’histoire de notre pays, toutes les conquêtes sociales ont pu être obtenues quand les syndicats étaient unis”. Oui, l’unité dans le lutte est le seul moyen de gagner. Syndicalement, et politiquement.**

Emmanuel Macron est le président des riches. Destruction des services publics, casse de l’hôpital public, mutilation de la sécurité sociale, cadeaux fiscaux aux plus riches, suppression de l’ISF, baisse des APL, etc. C’est le Thatcher du 21e siècle. Face à une telle violence sociale, un remède existe : l’unité ! (Pour qu’il n’y ait pas ambiguïté, le PS ne fait pas parti du périmètre de l’unité dont il est question dans ce billet.).

![images/forbes.png]

### 7 Français sur 10 veulent une mobilisation unitaire de la gauche et des syndicats.

Selon un sondage "L'opinion en direct" dirigé par Elabe publié aujourd’hui [voir], cette envie d’unité est d’ailleurs ce que réclament très majoritairement les français. D’après ce sondage, 68% des personnes interrogées souhaitent en effet “une union des forces politiques de gauche et des syndicats de salariés, ainsi que des actions communes de leur part en vue de s'opposer à la feuille de route gouvernementale”. Mais cela va plus loin. 63% des sympathisants de gauche appuient une alliance de la FI avec le PCF, 63% désirent aussi qu'une main insoumise se tende vers les écologistes. 62% de ces personnes appellent de leurs vœux un rapprochement avec Génération.s. Parmi les électeurs de la France Insoumise, 66% sont favorables à une alliance avec Génération.s, 64% avec EELV et 62% avec le PCF.

Bref, l’envie d’unité est très fort dans le pays et les sympathisants de gauche réclament l’alliance des forces politiques de gauche. Très franchement, ce n’est pas une surprise. Pour ma part, je n’ai jamais rencontré quelqu’un, lors d’une distribution de tract ou une réunion publique, qui m’ait dit : “je souhaite que vous vous divisiez, ça sera plus efficace”. Et ce bon sens, Pierre Laurent n’a eu de cesse de le marteler depuis des années et en particulier depuis 2015 où il appelait alors à construire une plateforme commune pour faire émerger une candidature unique de la gauche de transformation sociale et écologique pour l’élection présidentielle [voir]. Malheureusement, le PCF n’étant aujourd’hui pas suffisamment puissant pour être entendu, la division a eu lieu à la présidentielle, puis aux législatives puis partout depuis. En conséquence de quoi, les défaites se sont enchainées. Triste gâchis. Il est temps de réagir.

Alors que nous sommes dans une période d’ébullition et d’effervescence sociale, notre rôle, en tant qu’organisation politique, est de soutenir ensemble, en faisant bloc, les luttes sociales et les syndicats qui les portent. Car en effet, l’unité politique est le seul moyen d’éviter le piège des tentatives de récupération politique et partisane du mouvement social, ce qui serait dévastateur. D’ailleurs, appuyer dans l’unité les luttes en cours, c’est exactement ce que réclament les grévistes.

![images/img-0736.jpg]

Évidement, cette unité politique dans la lutte de doit pas se contenter d’être une juxtaposition de forces et de logos sans contenu. L’unité politique, toute comme l’unité syndicale, se construit pas à pas, en se mettant autour d’une table, en discutant sans éviter les sujets qui fâchent, puis en construisant sur ce qui nous rassemble plutôt que sur ce qui nous sépare. Bref, se mettre autour de la table, construire ensemble, organiser ensemble, et faire bloc. Tout cela est d’ailleurs déjà à l’oeuvre. Le 10 avril dernier, une réunion unitaire regroupant 12 organisations - Alternative Libertaire, EELV, Ensemble!, Ensemble Insoumis, GDS (G. Filoche), faisons la Fête à Macron (F. Ruffin et E. Vire), Générations (P. Cherki), groupe Assemblée nationale FI (C. Autain), Nouvelle Donne, PCF, PCOF, R&S - actait les décisions suivantes :

- écriture d’une déclaration unitaire pour les étudiants,

- accord pour faire un meeting commun en faisant intervenir des salarié.e.s et étudiant.e.s en lutte (malgré des réserves émises par le PG et EELV),

- accord pour un nouveau déplacement unitaire et paritaire,

- écriture d’un document de propositions communes pour la SNCF,

- mise en discussion de la proposition communiste de faire un référendum / votation populaire,

- nouvelle réunion unitaire rapide pour discuter des conditions de préparation de la manifestation nationale «  la fête à Macron » du 5 mai.

Depuis, les choses ont avancé. Un meeting commun a eu lieu lundi 30 avril place de la république. Un document commun sur la SNCF a été rédigé et signé par la plupart des organisations.

![images/tract.png]

Mais, que ce soit pour le tract ou pour la réunion publique, dans les deux cas, la France Insoumise a finalement préféré ne pas s’associer à cette construction collective.

Lors du meeting, ce besoin d’unité a été martelé par tous les intervenants, qu’ils soient travailleurs en grève ou responsables politiques. Ce soir là, Pierre Laurent plaidait pour l’addition des forces : "Dans ce combat, l'unité de toutes les professions, du public comme du privé, des retraités comme des étudiants, l'unité syndicale, l'unité de toutes les forces de gauche qui sont les initiatrices de ce rassemblement, etc. Tout cela, tous ces ingrédients seront nécessaires. Il n'y a pas de place pour la division. Tout doit s'additionner. Il faut additionner nos forces. Il faut additionner nos énergies. Additionner nos initiatives. Parce que face à un pouvoir aussi arrogant, aussi autoritaire et aussi brutal, il faudra que le peuple de notre pays montre son unité et sa force. Ensemble pour faire reculer Macron".

![images/dscn7359.jpg]

Quelque peu agacé de voir de certains se désolidariser au dernier moment de ce dispositif unitaire, Olivier Besancenot enfonçait alors le clou : "Ceux qui ne comprennent pas que l'unité est essentielle dans ce conflit, ils reviendront de toute façon parce qu'ils n'auront pas le choix, parce qu'il n'y a pas de place pour le sectarisme. On a besoin d'une victoire sociale et politique. Et cette victoire, nous devons la gagner tous et toutes ensemble".

Mais visiblement, le message d’unité ne passe toujours pas. Sur BFMTV et sur sa dernière note de blog, Jean-Luc Mélenchon considérait que ce meeting unitaire avait été, je cite, “un mauvais coup porté à la mobilisation”. Hallucinant. Comme si la désunion allait aider.

D’ailleurs, nous savons tous que la victoire ne peut se construire que dans un processus de rassemblement. Pour mémoire, ce sont bien les meetings communs de la gauche qui ont permis de faire basculer l’opinion lors de la campagne pour le NON au traité constitutionnel européen en 2005. Et jamais aucune victoire sociale n’a été possible dans la division. C’est une constante historique. La bonne méthode c’est de se mettre autour d'une table et organiser ensemble des initiatives unitaires en appui des luttes syndicales. C’est ça notre rôle. C’est ce que les syndicats demandent. C’est ce que les français demandent. Tout autre chemin de lutte nous enverra tous dans le mur. A bon entendeur...

