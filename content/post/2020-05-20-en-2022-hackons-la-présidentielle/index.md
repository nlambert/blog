---
title: En 2022, hackons la présidentielle !
author: Nicolas Lambert
date: '2020-05-20'
slug: en-2022-hackons-la-présidentielle
categories: []
tags:
  - '2020'
  - présidentielle
description: Pour rompre dès maintenant avec les logiques de la Ve République, imposons une candidature collective de la gauche à l’élection présidentielle de 2022.
featured: yes
toc: no
featureImage: 
thumbnail: images/access-granted-590x332.jpg
shareImage: images/access-granted-590x332.jpg
codeMaxLines: 10
codeLineNumbers: no
figurePositionShow: yes
---

Il n’y aura pas de victoire possible de la gauche en 2022 sans rassemblement. Le rassemblement de tous ceux qui n’ont eu de cesse de combattre la politique d’Emmanuel Macron depuis son élection ; le rassemblement de tous ceux qui ont combattu les politiques régressives menées pendant le quinquennat Hollande. Insoumis, Communistes, écologistes, syndicalistes, Gilets jaunes, anti-capitalistes, progressistes, militants associatifs et tous ceux qui ont combattu à gauche le TCE en 2005, nous sommes majoritaires dans le pays. C’est ensemble qu’il faut conquérir le pouvoir.

Pour cela, il va nous falloir déverrouiller un scrutin conçu comme la « rencontre d’un homme et d’un peuple ». Ce modèle dépassé et archaïque pensé par De Gaulle et pour De Gaulle dans un contexte bien particulier, n’a d’ailleurs jamais convenu à la gauche qui n’est pas à l’aise, et c’est tant mieux, avec la culture du chef. Donc plutôt que de considérer que pour prendre le pouvoir en Ve république, il faut en épouser les principes, ce texte appelle à subvertir un système électoral qui ne cesse de biaiser le jeu démocratique. En d’autres termes, n’attendons pas de prendre le pouvoir pour rompre avec les logiques de la monarchie présidentielle.

Pour cela, il faut frapper les esprits et refuser de présenter un candidat à l’élection présidentielle mais nous y présenter collectivement. En d’autres termes, représenter ensemble un programme construit en commun pour la présidentielle et les législatives. Pour qu’un tel rassemblement fonctionne, il faut la conjonction de deux critères.

### Un programme clair de transformation sociale et écologique.

Quand c’est flou c’est qu’il y a un loup. Un tel rassemblement devra donc se faire sur un contenu clair. Pour cela, une plateforme commune doit être mise en place au plus vite. Celle-ci doit être ouverte aux forces politiques mais aussi à tous ceux qui souhaitent s’engager dans cette initiative. Un calendrier de travail doit être défini ainsi que des règles démocratiques pour trier les propositions. Notons que les outils numériques modernes permettent de faire ce travail en toute transparence. On peut aussi imaginer la mise en place d’un comité de liaison permanent entre les différentes forces politiques engagées dans ce processus. A terme, ce travail devra conduire à l’élaboration d’un socle de propositions qui rompe avec 40 ans de logiques libérales. Nous en connaissons d’ailleurs déjà les contours : planification écologique, réduction du temps de travail, hausse des salaires, égalité salariale femmes-hommes, relocalisation de la production industrielle, retraite à 60 ans, restauration de l’ISF, rupture avec les traités européens, sortie de l'OTAN, développement des services publics, etc. Nous ne partons pas de rien. Certes, des désaccords devront être tranchés et ils devront l’être clairement. Il y aura du débat et de la conflictualité. Mais il ne faudrait pas que des sujets secondaires servent de prétexte à la dislocation. Je pense ici par exemple au nucléaire [voir].

### Un rassemblement le plus large possible.

Un tel rassemblement ne pourra et ne devra se faire autour de ceux qui ont soutenu de près ou de loin la politique menée pendant le quinquennat Hollande. On rejoint ici la nécessité de clarté évoquée plus haut. On voit mal en effet comment des gens qui ont soutenu la Loi El Khomri alors que nous, nous étions dans la rue en train de la combattre, pourraient faire parti d’un tel processus. Ceci est une évidence. Néanmoins, ce rassemblement doit être le plus large possible et se faire sans exclusives à gauche. A ceux qui considèrent par exemple que des socialistes ne peuvent pas faire partie d’un tel rassemblement, je rappelle quand même que nombre d’entre vous (c’est une critique récurrente) considèrent que la défaite de Jean-Luc Mélenchon en 2017 est due à la présence de Benoît Hamon. Je rappelle aussi que Jean-Luc Mélenchon avait proposé à Arnaud Montebourg d’être son premier ministre en cas de victoire et à Benoît Hamon d’être son ministre [voir]. Bref, l'union est une force propulsive. Et concevoir un rassemblement trop étriqué produira forcément des candidatures alternatives et amenuisera nos chances de victoire. Revire un duel de second tour Macron – Le Pen serait une catastrophe. Alors voyons quel pacte commun il est possible de défendre ensemble à gauche. Le périmètre précis de l'union se définira de fait, au cours du processus. Mais ne soyons pas naifs, si le contenu est réellement en rupture avec les logiques libérales, il est peu probable que le PS y participe. Cela aura le mérite de la clarté. 

Cette solution collective comporte évidemment des difficultés. Il ne faut pas les sous-estimer. En premier lieu, il ne faudrait pas qu'il se résume à un cartel d'organisations. Pas de victoire possible à gauche sans implication populaire. Il va falloir aussi contenir les égos démesurés de ceux qui considèrent qu’ils sont LA solution et qui s’imaginent être des hommes d’états « au dessus de la mêlée » (oui, ce sont rarement des femmes).  Il va falloir ne pas céder à la tentation de monter en épingles des sujets secondaires. Et il va falloir imposer cette idée dans les médias qui ne cesseront de nous demander qui est le vrai candidat et de pointer du doigts nos désaccords. On trouvera d’ailleurs toujours des personnes, y compris dans notre camps, pour dire que ce n’est pas possible. Et in fine, il nous faudra bien trouver un nom à inscrire sur les bulletins de vote sans que cela ne brise la logique collective de cette candidature. Pendant la campagne, il faudra donc faire la démonstration que ce que nous proposons fonctionne. Il nous faudra rompre avec les logiques tribunitiennes et renouer avec l’idée des meetings communs pour permettre à chaque force d’être présente dans l’élection. Ceux-ci pourraient être animés par exemple par les candidats aux législatives. Les différences entre nous sont d’ailleurs plutôt une force si nous sommes capables de défendre ensemble un socle de propositions communes. Car en cas de victoire, imaginez un gouvernement où un ministre insoumis de l’économie serait chargé d’organiser la planification écologique. Un gouvernement où un ministre communiste chargé du travail et de l’emploi serait chargé de mettre en place la sécurité d'emploi et de formation pour en finir avec les logiques de marché du travail [voir]. Et tout cela pendant que l'assemblée constituante travaille à bâtir les contours d'une VIe République...

Et sinon ? Quelle est l’autre alternative ? La confrontation des egos ? La course des petits chevaux ? La division ? Ne l’oublions jamais, l’histoire des grands hommes c’est l’histoire de l’homme qui cache la forêt. Alors hackons le système. Créons dès maintenant une rupture radicale avec le système que nous combattons. 2022 sera collectif ou ne sera pas !
