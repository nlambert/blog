---
title: Plan A et plan B
author: Nicolas Lambert
date: '2020-02-25'
slug: plan-a-et-plan-b
categories: []
tags: []
description: Article description.
featured: yes
toc: no
featureImage: 
thumbnail: images/ab.jpg
shareImage: images/ab.jpg
codeMaxLines: 10
codeLineNumbers: no
figurePositionShow: yes
---

**A deux mois du premier tour de l’élection présidentielle, je propose mon plan A et mon plan B.**

Je crois que ni Hamon ni Mélenchon ne sont prêts à faire les pas nécessaires pour aller vers un rapprochement. L'un veut effectivement tuer le PS car c'est selon lui le seul moyen de relancer une dynamique de progrès social. Et l'autre n'a qu'un objectif, prendre la tête d'un PS en crise pour reconstruire sur les cendres après la défaite. Moi ce que je souhaite, c'est que la gauche qui veut rompre avec le Hollandisme s'unisse sur un pacte clair, pour avoir une chance d’accéder au pouvoir. Mais, en tout état de cause, si jamais l'unité ne se fait pas, je soutiendrai sans hésiter une seule seconde celui dont je considère qu'il a le programme le plus à même à répondre aux enjeux du pays : Jean-Luc Mélenchon.

### Ma stratégie électorale s’établit donc en deux temps : 

PLAN A : j'utiliserai toute mon énergie pour qu'on construise un pacte de majorité reposant sur un contenu clair et regroupant tous les acteurs de la gauche de transformation sociale qui veulent rompre clairement avec le Hollandisme. C'est probablement le seul moyen de gagner.

PLAN B : Si l'union ne se fait pas (ce qui est probable), je soutiendrai bec et ongles la candidature de Jean-Luc Mélenchon dont les propositions sont les plus à même de sortir le pays de la crise sociale et écologique que nous traversons. Dans ce cas là, les électeurs de gauche devront choisir clairement entre un Hamon qui a pris part au gouvernement Hollande et un Mélenchon qui a pris ses distances avec le PS depuis 8 ans.
