---
title: Cartographies du vote des communistes
author: Nicolas Lambert
date: '2018-10-18'
slug: cartographies-du-vote-des-communistes
categories: []
tags:
  - cartographie
description: Article description.
featured: yes
toc: no
featureImage: 
thumbnail: images/texte3.png
shareImage: images/texte3.png
codeMaxLines: 10
codeLineNumbers: no
figurePositionShow: yes
---

**Les 4, 5 et 6 octobre derniers, les communistes étaient appelés à se prononcer pour désigner le texte qui servira le base commune de discussion pour le prochain congrès. 4 textes étaient en competition. Voici le résultat du vote en cartes.**

### Carte 1 - les votants

Lors de ce vote qui a duré 3 jours, il y a eu au total 30 999 votants (sur 49 467 inscrits), soit un taux de participation de 61%. 661 personnes ont voté blanc ou nul. Le nombre de votes exprimés dépasse 500 personnes dans 15 départements (noms indiqués sur la carte). Précisions que seuls les adhérents de plus de 3 moins à jour de cotisation pouvaient voter. Et précisons aussi qu’il n’était pas possible de voter sur Internet, car “l’Humain d’abord”, ca ne se dématérialise pas ;-)

![images/votants.png]

### Carte 2 - les textes arrivés en tête

Le texte 1 est arrivé en tête dans 49 départements. Le texte 2 est arrivé en tête dans 4 départements. Le texte 3 est arrivé en tête dans 40 départements. Le texte 4 est arrivé en tête dans 3 départements. Au total, un des 4 textes a obtenu plus de 50% des suffrages exprimés dans 55 départements (dont les noms sont inscrits sur la carte).

![images/textes.png]

### Carte 3 - Le communisme est la question du XXIe siècle

Ce texte (arrivé en 2e position) a recueilli 11 472 voix, soit 37.8% des suffrages exprimés. Il a la majorité absolue (plus de 50% des suffrages exprimés) dans 28 départements.

![images/texte1.png]

### Carte 4 - Pour un printemps du communisme

Ce texte (arrivé en 3e position) a recueilli 3 610 voix, soit 11.9% des suffrages exprimés. Il a la majorité absolue (plus de 50% des suffrages exprimés) dans 1 département.

![images/texte2.png]

### Carte 5 - Manifeste du Parti communiste du XXIe siècle

Ce texte (arrivé en tête) a recueilli 12 749 voix, soit 42% des suffrages exprimés. Il a la majorité absolue (plus de 50% des suffrages exprimés) dans 24 départements.

![images/texte3.png]

### Carte 6 - Reconstruire le Parti de classe

Ce texte (arrivé en 4e position) a recueilli 2 507 voix, soit 8.3% des suffrages exprimés. Il a la majorité absolue (plus de 50% des suffrages exprimés) dans 2 départements.

![images/texte4.png]





