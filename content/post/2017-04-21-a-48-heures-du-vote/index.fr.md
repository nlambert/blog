---
title: A 48 heures du vote
author: Nicolas Lambert
date: '2017-04-21'
slug: a-48-heures-du-vote
categories: []
tags:
  - Présidentielle
description: Article description.
featured: yes
toc: no
featureImage: 
thumbnail: images/e103b04fae70ec04dad2a0fd57f66318.jpg
shareImage: images/e103b04fae70ec04dad2a0fd57f66318.jpg
codeMaxLines: 10
codeLineNumbers: no
figurePositionShow: yes
---

**A moins de 48 heures du premier tour de l’élection présidentielle, j’invite chacun d’entre vous à la réflexion.**

A moins de 48 heures du premier tour de l’élection présidentielle, j’invite chacun d’entre vous à la réflexion. Vous avez tous été abondamment inondés sur les réseaux sociaux d’arguments, de contre arguments, de polémiques et j’y ai d’ailleurs moi-même largement contribué. Peut être trop. A présent, tous les arguments sont sur la table. Quels que soient les chiffres donnés par les derniers sondages, les éditos de dernière minute ou les événements, aussi tragiques qu’ils soient, j’invite chacun à prendre un peu de recul avant le vote. Et pour cela, je vous invite à la réflexion. Quel est le bon choix ? Quel avenir voulons nous dessiner pour le pays ? Quel chemin devons-nous emprunter ? Cette réflexion doit être globale. Pour cela, je vous invite donc à vous prononcer non pas en cherchant à savoir ce qui est bon pour vous, mais en cherchant à savoir ce qui est bon pour tous. Ce qui est au cœur de cette réflexion, c’est l’intérêt général (qui est autre chose que la somme intérêts particuliers). L’esprit de la République se fonde dans cette démarche visant à rechercher l’intérêt général. Qu’est ce que je crois bon pour tous ? Quel destin commun devons-nous construire ensemble ? Quel avenir partagé ? Répondre à ces questions est parfois compliqué. Nous sommes tous tiraillés par des contradictions difficiles à dénouer. Mais notre rôle de citoyen est de faire ce travail exigeant et de nous prononcer librement dans le secret de l'isoloir. Au cœur de cette réflexion intime, de nombreux sujets concrets sont à prendre en compte : Quel sort pour les étrangers ? Y-a-t-il une limite à l’accumulation de la richesse ? Comment protéger notre planète ? Quels droits nouveaux devons-nous nous donner ? La liste est infinie. A chacun ses questions, ses affects, ses sensibilités, ses interrogations. Mais au final, une chose est certaine, votez selon vos convictions profondes. Pas de vote tactique, pas de vote stratégique, pas de vote de rejet. Un vote de conviction ! En tout cas, moi, c’est ce que je ferai. On se retrouve de l’autre côté. Pour le meilleur ou pour le pire...



