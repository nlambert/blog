---
title: Les 500 familles
author: Nicolas Lambert
date: '2017-12-17'
slug: les-500-familles
categories: []
tags:
  - infographie
description: Article description.
featured: yes
toc: no
featureImage: 
thumbnail: images/graph1-v2.png
shareImage: images/graph1-v2.png
codeMaxLines: 10
codeLineNumbers: no
figurePositionShow: yes
---

**Une infographie qui montre de façon claire, en quoi les grandes fortunes françaises s’enrichissent pendant que les classes populaires subissent des politiques austérité. Une sorte de vase communiquant entre pauvres et riches. Du ruissellement à l'envers... On m’aurait menti ?**

![images/graph1-v2.png]


