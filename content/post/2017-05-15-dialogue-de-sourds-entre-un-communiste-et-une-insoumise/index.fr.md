---
title: Dialogue de sourds entre un communiste et une insoumise
author: Nicolas Lambert
date: '2017-05-15'
slug: dialogue-de-sourds-entre-un-communiste-et-une-insoumise
categories: []
tags: []
description: Article description.
featured: yes
toc: no
featureImage: 
thumbnail: images/conversation.jpg
shareImage: images/conversation.jpg
codeMaxLines: 10
codeLineNumbers: no
figurePositionShow: yes
---

**Ce billet relate un échange informel (sur facebook) entre un communiste (moi même) et une militante insoumise très active sur les réseaux sociaux. Ce dialogue n'engage que nous deux mais me semble révélateur des difficultés que rencontrent nos deux organisations à s'entendre pour les législatives. Je le publie ci-dessous avec son accord.**

**Un communiste (moi)** : Moi je crois que pour gagner, toutes les forces à la gauche du PS doivent se coaliser. Car si nous arrivons au pouvoir, nous serons ensemble dans la majorité et ensemble au gouvernement. Bref, s'affronter est absurde. Mais force de nous taper dessus, le sillon de la division de creuse.

**Une insoumise* **: Tant que la PCF n'aura pas théorisé sa rupture totale avec le PS (pseudo-frondeurs inclus) rien ne sera possible.

**Un communiste (moi)** : Et vous avec EELV ?

**Une insoumise** : Nous, on a commencé à faire de la politique autrement et ça ne changera plus désormais. Plus jamais de magouilles ni d'accords de coins de table entres les instances de partis ! Certains partis souhaitent rejoindre le mouvement citoyen FI ok mais qu'ils ne tentent surtout pas d'en tenir les rênes !

**Un communiste (moi)** : Moi je ne propose ni magouille ni accords de coins de table. Juste des accords portant sur un contenu programmatique précis. Mais investir n'importe qui du moment qu'il signe une charte, la voilà la magouille. 

**Une insoumise** : Cette charte, en plus d'être éthique , ne signifie qu'une chose : engagement des élus à respecter le programme pour lequel les électeurs ont voté ! C'est la moindre des choses non ?

**Un communiste (moi)** : Non. Les communistes ont fait une campagne autonome et nous avons mis tout notre poids dans la bataille. Aux législatives, je veux défendre la SEF, de nouveaux droits dans entreprises, etc. Et je ne veux pas de votre proposition qui consiste à financer la secu avec la CSG. Mais vous, vous êtes comme Macron, vous croyez que chaque vote est une adhésion totale à votre programme....

**Une insoumise** : Le financement de la sécu avec la CSG est dans le programme de la FI et est une mauvaise idée à votre avis ? Je ne me prononcerai pas. Mais bon sang de bonsoir, pourquoi ne pas être venu l'expliquer pendant les 8 mois où nous avons travaillé sur ce programme ? On n'attendait que vous, après des auditions programmatiques avant de prendre une décision consensuelle ! C'est facile de venir critiquer après que le job ait été fait.

**Un communiste (moi)** : Oui c'est une mauvaise idée. En 2012, nous étions contre. En 2011, pendant que nous élaborions ensemble le programme le PG proposait ça, puis sous la pression des communistes, le PG est revenu dessus. Nous proposions même de supprimer la CSG. Puis, Mélenchon s'est remis à osciller entre notre position et la position initiale du PG. Récemment, dans un article paru dans le quotidien du médecin, vous nous avez piqué tout ce qu'il y a dans les bouquin du PCF mais sans modifier pour autant le livret programmatique en question. Bref, cela fait plus de 5 ans que nous échangeons la dessus. Mais une chose est sure, finance la sécu avec le CSG c'est un moyen de dédouaner les patrons de leur responsabilité. Et ça, c'est inacceptable pour nous !

**Une insoumise** : Il fallait l'expliquer et venir argumenter au bon moment !

**Un communiste (moi)** : Alors là tu te moques de nous. Vous connaissez la position du PCF, ils ont écrit 1000 fois dessus. Mais ce n'est pas la votre, vous voulez fiscaliser la sécu, assumez-le.

**Une insoumise** : Vous vous êtes tenus volontairement à l'écart de ce mouvement citoyen et de son travail. Toutes les propositions ont été présentées, commentées, discutées, amendées et votés. Où étiez-vous pendant ce temps là sinon à continuer à faire les yeux doux aux socialistes ?

**Un communiste (moi)** : Dès 2015 nous lancions un chantier programmatique qui s’est concrétisée par une petite brochure largement perfectible. Mais c’était une invitation au débat, à la co-construction. Cette main, vous ne l'avez jamais saisie. Car votre stratégie était claire : "fédérer le peuple" (NdT aucune alliance avec un autre parti politique). http://contribuer.projet.pcf.fr/

**Une insoumise** : Un programme pour qui ? pour quoi ? pour un nouvel cartel de partis ? Non merci !

**Un communiste (moi)** : Tu vois, sans même lire, tu rejettes d'emblée l'idée d'un accord programmatique. C'est la preuve qu'on ne peut pas parler du fond.

**Une insoumise** : C'est clair. Tant que vous continuerez à privilégier imperturbablement la survivance de votre parti et de son fonctionnement rigide et sclérosé à l'intérêt général, rien ne sera possible.

**Un communiste (moi)** : Mais moi c'est de la secu que je te parle, je te parle du fond, du projet, du programme. Franchement, c'est saoulant, jamais il n'est possible de parler de ça. Pourquoi voulez-vous aller vers une fiscalisation de la sécu ?

**Une insoumise** : Je ne fais pas partie de ceux qui ont approfondi cette partie du programme. Adressez-vous à eux... Je vais rechercher de qui il s'agit. Mais ils vont sûrement avoir la même réaction que moi. Pourquoi maintenant que tout est joué ?

**Un communiste (moi)** : Tout simplement parce que vous conditionnez toute alliance à la signature de la charte. Or celle-ci dit que si on signe, c'est dorénavant votre programme que nous devons défendre et voter à l'AN. Or comme tu le vois, nous avons des points de divergence assez forts. Et en gros, quand vous nous demandez de signez, on à l'impression que vous voulez nous tordre le bras pour qu'on renonce à nos propositions. Et la sécu (notamment), ce n'est pas une mince affaire pour nous. Tu comprends ? Ce qu'il faut, ce sont des alliances dans le respect de chacun. Mais quand on dit ça, vous dites "carabistouilles", "magouilles", "accords de coins de table", etc... Bref, on ne peut parler de rien :-(

**Une insoumise** : Pas d'alliance de partis. C'est dans l'ADN du mouvement. Terminé ça !

**Un communiste (moi)** : Et oui. Donc cessez de dire que si les alliances n'ont pas lieu, c'est de notre faute !

**Une insoumise** : Nous NE voulons PAS (PLUS JAMAIS) d'alliances. Nous ne vous accusons donc pas de ça. Notre programme constitue un tout. Cohérent. Pas découpable en tranches négociables.

**Un communiste (moi)** : Ok, c'est noté. Merci pour la clarification
