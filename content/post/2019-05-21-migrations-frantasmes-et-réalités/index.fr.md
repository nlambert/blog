---
title: Migrations, frantasmes et réalités
author: Nicolas Lambert
date: '2019-05-21'
slug: migrations-frantasmes-et-réalités
categories: []
tags:
  - migrations
description: Article description.
featured: yes
toc: no
featureImage: 
thumbnail: images/confpcf.png
shareImage: images/confpcf.png
codeMaxLines: 10
codeLineNumbers: no
figurePositionShow: yes
---

**La question migratoire est le l’objet de bien des débats. Quelle est la part de fantasmes ? Quelle est la réalité ? A quelques jours des européennes, Cette vidéo dresse l'état des lieux.**

La question migratoire est le l’objet de bien des débats. Elle sera à coup sur, avec la question du pouvoir d’achat, au cœur de la campagne des élections européennes. Qu’en est-il de la réalité géographique des migrations internationales aujourd’hui. Y-a-t-il une crise migratoire ? Quelles sort est réservé aux migrants qui arrivent en Europe ? Quid de ceux qui échouent dans leur périple ? Ou se déploient les solidarités ? Cette présentation visera à faire, cartes géographiques à l’appui, l’état des lieux de ces questions ô combien cruciales.

{{< youtube LTNgh6JOK8k >}}


