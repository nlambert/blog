---
title: Petite cartographie des candidats
author: Nicolas Lambert
date: '2017-04-11'
slug: petite-cartographie-des-candidats
categories: []
tags:
  - Présidentielle
  - infographie
description: Article description.
featured: yes
toc: no
featureImage: 
thumbnail: images/carto-candidats.png
shareImage: images/carto-candidats.png
codeMaxLines: 10
codeLineNumbers: no
figurePositionShow: yes
---

**A moins de deux semaines du premier tour, je vous propose ici une petite « cartographie » des principaux candidats à l’élection présidentielle.**

![images/carto-candidats.png]

A moins de deux semaines du premier tour, je vous propose ici une petite « cartographie » des principaux candidats à l’élection présidentielle. Dans cette élection, trois projets de société - résumés ici en image - sont en compétition : le libéralisme austéritaire d’Emmanuel Macron et de François Fillon, le nationalisme de Marine Le Pen et le républicanisme social de Jean-Luc Mélenchon. 

Clef de lecture : les variations de forme représentent la diversité de la population ;  les variations de taille représentent l'inégale répartition des richesses. L'emplacement des figurés graphiques à l’intérieur de l’hexagone (représentant la France) est aléatoire.

