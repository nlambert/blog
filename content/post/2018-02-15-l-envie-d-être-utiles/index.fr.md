---
title: L’envie d’être utiles
author: ''
date: '2018-02-15'
slug: l-envie-d-être-utiles
categories: []
tags: []
description: Article description.
featured: yes
toc: no
featureImage: /images/path/file.jpg
thumbnail: /images/path/thumbnail.png
shareImage: /images/path/share.png
codeMaxLines: 10
codeLineNumbers: no
figurePositionShow: yes
---

**On reproche souvent au parti communiste ses alliances locales. Pourtant, loin de tout reniement, ces pratiques politiques relèvent avant tout d’une volonté d’être utile. De faire plutôt que de dire.**

Le parti communiste est un parti de gauche, marxiste et anticapitaliste, qui défend une politique de progrès social et de dé-financiarisation de l’économie, en rupture totale avec les politiques libérales d’austérité actuelles. Pour cette raison, aucune alliance ni aucun compromis n’a donc été possible avec François Hollande et encore moins avec Emmanuel Macron dont la conversion au néolibéralisme est plus que consommée. Nous sommes donc dans l’opposition de gauche, nous menons la bataille à l’assemblée contre tous les reculs sociaux, et cela depuis bien longtemps. Tous les votes, les amendements et les propositions de loi de nos parlementaires en attestent, merci de ne pas l’oublier.

Mais localement, les communistes ne se contentent pas de contester stérilement en agitant les bras et en assénant des punchlines sur twitter. Ils veulent agir ! Et c’est bien cette envie de faire, qui conduit souvent les communistes à effectuer des alliances avec les autres forces de gauche avec qui il y a évidemment de sérieux désaccords. Ces alliances locales se font donc toujours sur la base d’un projet politique concret, discuté et débattu par les militants, qui dépend évidemment d’un rapport de force local (les alliances ne sont donc pas toujours possibles, ni même souhaitables). Est-ce un alignement ou un reniement ? Non ! C’est une façon d’agir et de mener la bataille politique en s’en donnant les moyens concrets. Est-ce la "lutte des places", un moyen de s’enrichir ? Non ! Au PCF personne ne s’enrichit avec la politique puisque les indemnités d’élus sont reversées au parti. Est-ce que c’est dur ? Oui, personne ne peut dire le contraire ! Est-ce qu’on obtient satisfaction sur tout ? Bien sur que non. On avale parfois des couleuvres. Mais qu’ont obtenu de concret ceux qui nous accablent de reproches et qui refusent toute alliance avec qui que ce soit. Ont-ils obtenu des avancées ? Ont-ils empêché des reculs ? Rien de rien !

https://twitter.com/Qofficiel/status/953708747704143872

Évidement, cette stratégie du rassemblement portée par les communistes, qui n’est d’ailleurs pas nouvelle, est loin d’être parfaite. Les alliances locales avec un PS qui s’est fourvoyé dans des politiques libérales et anti-sociales au niveau national doivent être plus que jamais questionnées. Est-il possible de s’allier avec ceux qui ont soutenu par exemple Manuel Valls aux primaires socialistes ? Certainement pas. Avec ceux qui ont soutenu Benoit Hamon. Peut être. Avec ELLV ? Cela dépend des situations locales. Pas si simple... Mais une chose est certaine : le refus systématique de participer à toute coalition ne donne pas les moyens d’agir, ni même de s’opposer efficacement. Ou alors, j’attends qu’on me démontre le contraire...

Est-ce qu’on pourrait faire mieux ? Évidement. Mais que voulez vous, le PCF ne pèse plus assez lourd aujourd’hui pour enrayer le fait que la France s’enfonce depuis plus de 30 ans dans des régressions sociales à la chaîne. Pourtant, dans notre histoire, quand le PCF est fort dans notre pays, ça va mieux pour tout le monde. Sécurité sociale, congés payés, retraites, statut des fonctionnaires, loi SRU, etc., aucun progrès social en France n’a été réalisé sans le parti communiste. Raison pour laquelle, j’ai décidé, et cela depuis plus d’un an maintenant, de m’engager dans ce parti auquel je crois beaucoup.

Le sens de mon engagement

Pour moi, le communisme c’est :

- Une boussole : l'Humain d'abord comme guide de l'action politique ;
- Un horizon : la sortie du capitalisme ;
- Trois piliers : la lutte des classes, la révolution, l'internationalisme ;
- Une méthode : le rassemblement et la volonté de construire avec d’autres, des arcs de lutte majoritaires, contre la droite et l’extrême droite et pour le progrès social.

Le poing levé et la main tendue, c’est le sens de mon engagement.
