---
title: 'Le Plan A échoue, passons au plan B '
author: Nicolas Lambert
date: '2017-03-08'
slug: le-plan-a-échoue-passons-au-plan-b
categories: []
tags: []
description: Article description.
featured: yes
toc: no
featureImage: 
thumbnail: images/axes-pcf-1.png
shareImage: images/axes-pcf-1.png
codeMaxLines: 10
codeLineNumbers: no
figurePositionShow: yes
---

**Pour ne pas finir avec un second tour Macron – Le Pen, et pour que la gauche ait une chance réelle de gagner cette élection, la stratégie doit être réadaptée. Le plan A est en train d’échouer, il est plus que temps de passer au plan B. Comment faire ?**

Après plus d'un an de campagne à sillonner le territoire, Jean-Luc Mélenchon est passé de 11% d'intention de vote en février 2016 à... 10% dans un dernier sondage paru ce mardi. Si la dynamique est perceptible chez les militants, dans les meetings, sur les réseaux sociaux, sur youtube et au sein des groupes d'appui, force est de constater que le mouvement ne s'ancre ni dans le territoire ni dans les têtes. En effet, la stratégie de la France insoumise, censée « dépasser » les partis (comme EELV pensait pouvoir le faire en 2010) et construite initialement pour affronter l’austéritaire Hollande (ou Valls), a du mal a trouver une cohérence face au frondeur Hamon sorti vainqueur des primaires socialistes. Les deux candidatures de gauche ont tendance à se chevaucher et se neutraliser. Une chose est donc sure, pour ne pas finir avec un second tour Macron – Le Pen, et pour que la gauche ait une chance réelle de gagner cette élection, la stratégie doit être réadaptée. Le plan A est en train d’échouer, il est plus que temps de passer au plan B. Comment faire ?

Jean-Luc Mélenchon est indéniablement le meilleur candidat pour porter la rupture avec le quinquennat Hollande. En tout cas, il est évidemment plus crédible qu’un ancien ministre qui a manœuvré pour propulser Valls à Matignon et qui a soutenu Hollande une bonne partie du quinquennat, y compris quand étaient votées des mesures de régression sociale innaceptables (augmentation de la TVA, Traité budgétaire européen, ANI, CICE, etc...). Par ailleurs, Jean-Luc Mélenchon a été depuis 2005, de tous les combats dans la rue et dans les urnes. Ne doutons pas de ça, il mène la lutte haut et fort, et il la mène avec nous. Mais, la stratégie qu’il porte, et qui a été pensée à un moment donné où nous étions tous persuadés que Hollande  serait le candidat socialiste à l’élection présidentielle, n’est plus efficiente et doit être révisée. En d’autres termes, pour avoir une chance de figurer au second tour, toutes les forces de la gauche de transformation sociale doivent se fédérer !

Or, le cadre mis en place par la France insoumise ne permet pas ce rassemblement car il exclut de fait tous ceux qui ne veulent pas avaler tout rond et sans discuter le programme, la stratégie et le mode de fonctionnement du mouvement. Beaucoup de communistes, frondeurs et écologistes ne peuvent pas se reconnaître et se fondre dans un cadre où rien n’est négociable. Pourtant, faire front commun sur un certain nombre de propositions fortes est possible : 6e république, constituante, augmentation des salaires, dé-financiarisation de l’économie, création d’un pôle public bancaire, aller vers une sécu à 100 %, retraite à 60 ans avec 40 années de cotisations, plan de sortie des énergies carbonées, remplacement de la loi El Khomeri par une loi sécurité emploi formation (SEF), plan massif de lutte contre l’évasion fiscale, interdiction des licenciements boursiers, nouveaux droits dans l’entreprise, etc. L’idée n’est en aucun cas de créer du flou. En aucun cas ! L’idée est au contraire de construire et d’acter noir sur blanc un certain nombre d’engagements concrets et chiffrés que nous pourrions porter et défendre ensemble avec force. Bref, construire un arc de progrès social et écologique, ancré dans le peuple français lui même et susceptible de susciter l’adhésion des plus indécis. Car dans le pays, l’envie d’unité est très fort.

![images/axes-pcf-1.png]

En tout état de cause, si cette démarche de rassemblement ne permet pas d’avoir un candidat unique à l’élection présidentielle (le calendrier étant maintenant très serré), ce travail pourra permettre au moins de ne pas d’éparpiller nos forces aux élections législatives et sortir de la situation absurde qui aboutit à des affrontements sur le terrain entre des militants qui avaient su se rassembler en 2012. Bref, la France insoumise doit impérativement élargir son cadre de rassemblement, réviser sa charte et tendre la main aux autres forces de la gauche de transformation sociale pour que nous puissions mettre ensemble et collectivement un gros coup de pied sur l’accélérateur. En d’autres termes, la campagne a besoin d’un nouvel élan. Car nous devons être lucides sur la situation politique et pas aveuglés par l’enthousiasme militant qui nous anime tous. Car l’heure est grave. Oui, il faut tout faire pour sortir du Holandisme par la gauche. Mais éparpiller nos forces, laissera un boulevard à la droite et l’extrême droite dont la distance avec le pouvoir n’a jamais été si réduite. 