---
title: Les morts du capitalisme
author: Nicolas Lambert
date: '2017-12-21'
slug: les-morts-du-capitalisme
categories: []
tags:
  - infographie
description: Article description.
featured: yes
toc: no
featureImage: 
thumbnail: images/morts-du-capitalisme-1.png
shareImage: images/morts-du-capitalisme-1.png
codeMaxLines: 10
codeLineNumbers: no
figurePositionShow: yes
---

**Une infographie qui montre que le nombre de victimes du capitalisme est équivalent chaque année à celui de la seconde guerre mondiale**

Dans la préface du livre des frères Bocquet (Sans Domicile Fisc), Jean Ziegler se livre à un terrible constat : Marx s'est trompé ! De révolutions scientifiques en révolutions technologiques ou industrielles, aujourd'hui, l'humanité dispose de biens en quantité suffisante pour couvrir l'ensemble des besoins élémentaires de tous les hommes sur la planète. Ce que Marx croyait irréalisable dans un futur proche s'est finalement produit : le manque objectif est enfin vaincu. 

Mais ces biens sont inégalement répartis. Une oligarchie capitaliste composée d'une poignée de personnes, s'accapare les richesses et dicte sa loi aux états et aux citoyens. Les 500 plus grandes sociétés transcontinentales privées ont contrôlé en 2015, 53% de la richesse produite au cours de l'année sur l'ensemble de la planète. Mais de l'autre côté du spectre, un enfant de moins de 10 ans meurt de faim toutes les cinq secondes...

Face à ces faits insoutenables, Jean Ziegler en tire une conclusion tranchée : un enfant qui meurt de faim dans un monde plus riche qu'il ne l'a jamais été et qui a vaincu le manque objectif, est un enfant assassiné ! 

Au total, 54 millions de personnes seraient mortes pour la seule année 2015, du fait de cet inégal accès aux richesses, accaparées par quelques-uns. Un bilan aussi lourd que celui d'une guerre...

![images/morts-du-capitalisme-1.png]

Source : Sans Domicile Fisc, Les frères Bocquet (Eric et Alain), éditions Le cherche midi, 2016. Préface de Jean Ziegler