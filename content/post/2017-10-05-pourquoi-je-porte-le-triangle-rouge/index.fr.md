---
title: Pourquoi je porte le triangle rouge
author: Nicolas Lambert
date: '2017-10-05'
slug: pourquoi-je-porte-le-triangle-rouge
categories: []
tags:
  - Triangle rouge
description: Article description.
featured: yes
toc: no
featureImage: 
thumbnail: images/rouge.png
shareImage: images/rouge.png
codeMaxLines: 10
codeLineNumbers: no
figurePositionShow: yes
---

**Dans les camps de concentration, le triangle rouge était le signe cousu sur la veste des prisonniers politiques, celles et ceux qui se sont opposés à l'idéologie nazie. C’est aujourd’hui un symbole de la lutte contre l'extrême droite. Mais il est aussi aussi porteur de revendications sociales.**

Le triangle rouge renvoie au départ au système de marquage des déportés dans les camps de concentration nazis. Il était cousu sur les vestes des prisonniers politiques (communistes, résistants, objecteurs de conscience… ) ce qui permettait de les différencier des juifs (étoile jaune), des homosexuels (triangle rose), des tsiganes (triangle marron), des émigrés (triangle bleu), etc. Aujourd’hui, arborer ce triangle rouge c’est donc affirmer une opposition politique claire à la montée des idées d'extrême droite. Selon l’association belge "les Territoires de la Mémoire" qui est à l’origine de cette initiative, le porter, “c'est participer au cordon sanitaire citoyen pour une société libre, démocratique et solidaire”. Ce symbole “permet à chacun de rappeler discrètement que la plupart des citoyens refusent de céder aux idées haineuses, racistes, sexistes ou liberticides”. Bref, c’est un signe de résistance aux idées extrême droite.

![images/rouge.png]

Et évidemment, quand on arbore le triangle rouge, on le combat aussi dans les urnes. C’est donc sans hésiter que j’ai glissé un bulletin de vote Emmanuel Macron au second tour de l’élection présidentielle dans le but de réduire au le plus bas possible le score du Front National dans notre pays, et ainsi lui donner moins de force et moins d’aura. Car quand on dit combattre les idées extrême droite, il faut avoir les idées claires et ne pas tout mélanger...

{{< youtube JQd2AEDf2KM >}}

Mais le triangle rouge a une seconde signification. Il est également porteur d'une revendication ouvrière vieille de plus de 100 ans, les trois pointes du triangle représentant respectivement 8 heures de travail, 8 heure de sommeil et 8 heures de loisir. Lors de la première manifestation du 1er mai en 1890 à Paris, un triangle de cuir rouge avait été adopté pour porter cette revendication (la journée de huit heure sera enfin votée le 23 avril 1919). Ce symbole sera ensuite abandonné, remplacé par la fleur d’églantine puis par le muguet. .

![images/8hours1886chicago.jpg]

La boucle est bouclée. Avec le triangle rouge, les revendications sociales ouvrières viennent ainsi se superposer à la résistance aux idées d’extrême droite partout en Europe, et cela dans un geste de mémoire des résistants déportés par les nazis pendant la seconde guerre mondiale. Franchement, quel beau symbole !

En savoir plus : ![http://www.trianglerouge.be](http://www.trianglerouge.be)

