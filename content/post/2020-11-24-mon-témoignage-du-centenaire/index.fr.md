---
title: Mon témoignage du centenaire
author: Nicolas Lambert
date: '2020-11-24'
slug: mon-témoignage-du-centenaire
categories: []
tags:
  - PCF
  - centenaire
description: Article description.
featured: yes
toc: no
featureImage: 
thumbnail: images/100ans.png
shareImage: images/100ans.png
codeMaxLines: 10
codeLineNumbers: no
figurePositionShow: yes
---

**Pour ses 100 ans, le PCF invite ses militants à répondre à ces 3 questions.**

{{< youtube VyTBGt5BOaU >}}

1) Quel est ton prénom, ton âge, la ville et l’année où tu as adhéré, et surtout qu’est ce qui t’as poussé à faire ça.

2) Un évènement qui t’a marqué en tant que communiste. Une action, une victoire ou une défaite nationale, une rencontre, un combat, un temps fraternel, etc.

3) Le futur : Un camarade qui serait né en 1920 pourrait être arrière arrière grand-père. Et toi, quel monde tu veux pour tes arrière arrière petits enfants ? Quels objectifs devons nous collectivement mettre en place ?

Grande ou petite, à chacun de de raconter son histoire. Voici la mienne. 


