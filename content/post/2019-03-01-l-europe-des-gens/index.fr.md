---
title: L'Europe des gens
author: Nicolas Lambert
date: '2019-03-01'
slug: l-europe-des-gens
categories: []
tags:
  - europe
description: Article description.
featured: yes
toc: no
featureImage: 
thumbnail: images/dok3aqoxuaeuirr.jpg
shareImage: images/dok3aqoxuaeuirr.jpg
codeMaxLines: 10
codeLineNumbers: no
figurePositionShow: yes
---

**Le 26 mai prochain, on vote pour l'Europe des gens, pour la liste conduite par Ian Brossat ! #PCFisBack #EuropeDesGens**

J’ai adhéré au PCF il y a un peu plus de deux ans. Pour moi, l’histoire du communisme en France c’est la résistance contre les nazis, le front populaire, mai 68, les congés payés, la sécurité sociale, les retraites, le statut des fonctionnaires, les hausses de salaire, toutes les politiques sociales et culturelles au niveau local, c'est l’engagement de nombreux intellectuels, la lutte contre la colonisation, les combats pour la liberation de Nelson Mandela, les manifs contre la guerre,  etc. Quel parti peut en dire autant autant ? Du coup, j’en ai un peu marre, quand je dis que je suis communiste, qu’on me renvoie sans cesse à Staline, au Goulag, à l’URSS, etc... Ce n'est pas mon histoire. Et ce n'est pas non plus l'histoire du parti communiste français. Et nous, est-ce qu'on passe notre vie à parler aux capitalistes de leur histoire ? Pourtant, elle est peu glorieuse. Est-ce qu'on leur parle des enfants qu'ils ont envoyé au fond des mines, de la collaboration (lire à ce sujet L'Ordre du jour d'Éric Vuillard), de Pinochet, des regimes d’apartheid, de la colonisation, de l'esclavage, des deux guerres mondiales ? Est-ce qu'on leur rappelle que les Etats-unis en largué deux bombes nucléaires sur des civils à Hiroshima et Nagasaki ? Et quid des [morts du capitalisme](https://blogs.mediapart.fr/cartographe-encarte/blog/211217/les-morts-du-capitalisme) aujourd’hui ? Par ailleurs, concernant l’URSS, qui se souvient du référendum du 17 mars 1991 (80% de participation) où les habitants de l’Union soviétique se sont prononcés à 77,8 % pour rester dans l’union soviétique ? Et qui se souvient qu'alors, Boris Eltsine a fait un putsch en bombardant le parlement faisant au moins 1000 morts pour prendre le pouvoir ? Attention, l’Histoire est une matière malléable... Bref, je suis communiste, j'assume ce mot car je crois à l'idée de commun, aux valeurs de solidarité et de partage, même si ca ne fait pas chic. A bon entendeur. 

Au niveau européen, le PCF a toujours combattu la construction capitaliste de l’UE, incarnée par les traités en vigueur. Pour mémoire, le PCF a bataillé contre tous les traités sans oublier le traité de Maastricht en 1992. Avions-nous tort ?  N'aurait-il pas mieux fallu nous écouter ? Car ce « modèle » que nous avons combattu sans relâche a pour tare fondamentale de soumettre la construction européenne et les relations entre les peuples d’Europe au marché et à la concurrence, quitte à bafouer la souveraineté des peuples. Une impasse. 

Ce combat, nous continuons de le porter. Et nous le porterons à nouveau pour les prochaines élections européennes avec notre liste "l'Europe des gens". Cette liste conduite par Ian Brossat se veut à l'image de la société. Elle est composée de 50% de d’ouvriers et d’employés. Marie-Hélène Bourlard, deuxième sur la liste est une ouvrière du Nord. Notre ambition est de la propulser au parlement européen pour qu'elle devienne la première femme ouvrière à siéger dans l'hémicycle. Nous voulons rendre visible les invisibles. Donner la parole à celles et ceux qui luttent mais qui ne l'ont jamais. Je pense notamment à a Mamoudou BASSOUM, ce champion de Taekwondo qui s'est fait remarqué récemment en portant un gilet jaune sur  le podium. Il y a aussi Maryam MADJIDI, prix goncourt du 1er roman qui porte des postions fortes sur la question migratoire. Il y a aussi Benjamin AMAR, CGTiste de tous les combats, Elina DUMONT qui a été SDF pendant 15 ans, Arthur HAY, livreur Delivroo, Barbara FILHOL, aide soignante, Nacim BARDI, Ouvrier métallurgiste, etc. [On ne peut tous les citer](https://www.europedesgens.fr/vos_candidats)

Avec eux, nous nous battons pour mettre le social au cœur des politiques européennes, pour reprendre le pouvoir sur la finance, pour mener de front la transition écologique, pour développer les services publics. Rien ne se fera sans démocratie. Et rien ne se fera sans un PCF fort. L'histoire nous l'a montré. La reconquête est donc indispensable. Et elle est en marche. Hauts les cœurs !!! 

Si vous souhaitez participer ou soutenir cette campagne, inscrivez-vous sur le [site](https://www.europedesgens.fr/participez)

